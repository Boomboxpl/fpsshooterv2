﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa służąca do serializacji najwyższego wyniku.
/// </summary>
[System.Serializable]
public struct HighScore
{
    /// <summary>
    /// Numer fali.
    /// </summary>
    public int wave;
    /// <summary>
    /// Czas gry.
    /// </summary>
    public float time;

    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="_wave">Numer fali.</param>
    /// <param name="_time">Czas gry.</param>
    public HighScore(int _wave, float _time)
    {
        wave = _wave;
        time = _time;
    }
}
