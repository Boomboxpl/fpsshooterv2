﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/// <summary>
/// Klasa zarządzająca plikiem z najwyższymi wynikami.
/// </summary>
public class HighScoreLoader
{
    /// <summary>
    /// Nazwa pliku z zapisem.
    /// </summary>
    const string fileName = "HighScore.dat";
    /// <summary>
    /// Ścieżka do pliku z zapisem.
    /// </summary>
    static readonly string filePath = Application.persistentDataPath + "/" + fileName;

    /// <summary>
    /// Zapisanie wyniku do pliku.
    /// </summary>
    /// <param name="score">Wynik do zapisania.</param>
    public static void SaveHighScore(HighScore score)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, score);
        file.Close();
    }

    /// <summary>
    /// Odczytanie pliku z najwyższym wynikiem.
    /// </summary>
    /// <returns>Najwyższy wynik z pliku. Jeśli plik nie istnieje, zwraca wynik z samymi 0.</returns>
    public static HighScore LoadHighScore()
    {
        HighScore score = new HighScore(0, 0);

        if (File.Exists(filePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filePath, FileMode.Open);
            score = (HighScore)bf.Deserialize(file);
            file.Close();
        }

        return score;
    }

    /// <summary>
    /// Reset najwyższego wyniku. Usuwa plik z wynikiem.
    /// </summary>
    public static void ResetHighScore()
    {
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }
}
