﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Efekt flary.
/// </summary>
public class Flare : MonoBehaviour {
			
	/// <summary>
	/// Komponent światła.
	/// </summary>
	private Light flarelight;
	/// <summary>
	/// Komponent dźwięku.
	/// </summary>
	private AudioSource flaresound;
	/// <summary>
	/// Dźwięk flary.
	/// </summary>
	public AudioClip flareBurningSound;


	/// <summary>
	/// Inicjalizacja oraz rozpoczęcie.
	/// </summary>
	void Start () {

		flaresound = GetComponent<AudioSource>();
		flaresound.clip = flareBurningSound;
		flaresound.Play();
		flarelight = GetComponent<Light>();
		flaresound = GetComponent<AudioSource>();
	}
	
	
	/// <summary>
	/// Aktualizacja w każdej klatce. Zmiana intensywności światła.
	/// </summary>
	void Update () {
		flarelight.intensity = Random.Range(2f,6.0f);	
	}

}
