﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa komponentu dodającego efekt przetwarzania końcowego do kamery obiektu.
/// </summary>
public class TintCameraPP : MonoBehaviour
{
    /// <summary>
    /// Materiał z wybranym shaderem.
    /// </summary>
    public Material material;
    /// <summary>
    /// Kolor użyty do zmiany zabarwienia.
    /// </summary>
    public Color color;
    /// <summary>
    /// Wartość zmiany zabarwienia.
    /// </summary>
    [Range(0f,1f)]
    public float tintValue = 0;
    /// <summary>
    /// Przełącznik włączający automatyczne zanikanie intensywności.
    /// </summary>
    public bool isAutoDim;
    /// <summary>
    /// Szybkość zanikania intensywności.
    /// </summary>
    public float dimSpeed = 1;
    /// <summary>
    /// Krzywa określająca szybkość zaniku w czasie.
    /// </summary>
    public AnimationCurve fadingCurve;

    /// <summary>
    /// ID zmiennej w shaderze.
    /// </summary>
    private int tintValueID;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    private void Awake()
    {
        tintValueID = Shader.PropertyToID("_intensity");
    }

    /// <summary>
    /// Wykonanie przetwarzania na wygenerowanym obrazie.
    /// </summary>
    /// <param name="source">Obraz wygenerowany przez kamerę.</param>
    /// <param name="destination">Obraz po przetworzeniu.</param>
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.color = color;
        material.SetFloat(tintValueID, fadingCurve.Evaluate(tintValue));
        Graphics.Blit(source, destination, material);
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Sterowanie automatycznym zanikaniem intensywności.
    /// </summary>
    void Update()
    {
        if (isAutoDim)
        {
            if (tintValue < 0.001)
            {
                tintValue = 0;
                return;
            }
            tintValue -= tintValue * dimSpeed * Time.deltaTime;
        }
    }
}
