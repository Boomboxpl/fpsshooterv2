using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrarianLoader : MonoBehaviour
{
    public Texture2D terrainTexture;
    Terrain terrain;
    public float scale = 0.5f;
    public bool withNoise = false;

    private void Start()
    {
        SetupTerrarian();
    }

    [ContextMenu("Setup")]
    public void SetupTerrarian()
    {
        terrain = FindObjectOfType<Terrain>();
        var pixels = terrainTexture.GetPixels32();
        int mapHeight = terrain.terrainData.heightmapResolution;
        int mapWidth = terrain.terrainData.heightmapResolution;
        float stepX = terrainTexture.width / (float)mapWidth;
        float stepY = terrainTexture.height / (float)mapHeight;
        float[,] heights = new float[mapHeight, mapWidth];
        for(int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                heights[x, y] = terrainTexture.GetPixel((int)(x * stepX), (int)(y * stepY)).r * scale;
                if (withNoise)
                {
                    heights[x, y] = heights[x, y] + Mathf.PerlinNoise(x + 0.001f, y + 0.001f);
                    heights[x, y] = heights[x, y] / 2;
                }
            }
        }
        terrain.terrainData.SetHeights(0, 0, heights);
    }
}
