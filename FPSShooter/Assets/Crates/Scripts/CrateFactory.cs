﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstrakcyjna fabryka skrzynek.
/// </summary>
public abstract class CrateFactory : MonoBehaviour
{
    /// <summary>
    /// Metoda tworząca skrzynkę.
    /// </summary>
    /// <returns>Utworzona skrzynka.</returns>
    public abstract SupplyCrate CreateCrate();
}
