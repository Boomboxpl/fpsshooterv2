﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstrakcujna klasa skrzynki z zapasami.
/// </summary>
public abstract class SupplyCrate : MonoBehaviour
{
    /// <summary>
    /// Dźwięk podnoszenia.
    /// </summary>
    public AudioClip pickupSound;

    /// <summary>
    /// Szybkość początkowa spadania.
    /// </summary>
    public float fallingSpeed = 1;

    public float randomForceValues = 10000f;

    /// <summary>
    /// Sprawdzenie czy znajduje się na ziemi, lub innym blokującym obiekcie.
    /// </summary>
    public bool IsGrounded => collisionCount > 0;

    /// <summary>
    /// Komponent odtwarzający dźwięk.
    /// </summary>
    private AudioSource audioSource;
    /// <summary>
    /// Komponent ciała sztywnego.
    /// </summary>
    private Rigidbody rigidbody;
    /// <summary>
    /// Ilość aktualnie występujących kolizji.
    /// </summary>
    private int collisionCount;

    /// <summary>
    /// Abstrakcyjna metoda pozwalająca na wykonanie działania na podnoszącym graczowi.
    /// </summary>
    /// <param name="player">Podnoszący gracz.</param>
    public abstract void Resupply(Player player);

    /// <summary>
    /// Inicjalizacja. Ustawienie parametrów ciała sztywnego.
    /// </summary>
    public virtual void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = new Vector3(0, -fallingSpeed, 0);
        rigidbody.AddForce(new Vector3(Random.Range(-randomForceValues, randomForceValues), 0, Random.Range(-randomForceValues, randomForceValues)));
        collisionCount = 0;

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = pickupSound;
    }

    /// <summary>
    /// Oznaczenie skrzynki jako zużytej.
    /// </summary>
    protected void MarkAsUsed()
    {
        StartCoroutine(MarkingAsUsed());
    }

    /// <summary>
    /// Wykrycie wejścia w kolizję.
    /// </summary>
    /// <param name="collision">Infomracje o kolizji.</param>
    private void OnCollisionEnter(Collision collision)
    {
        collisionCount++;
    }

    /// <summary>
    /// Wykrycie wyjścia z kolizję.
    /// </summary>
    /// <param name="collision">Infomracje o kolizji.</param>
    private void OnCollisionExit(Collision collision)
    {
        collisionCount--;
    }

    /// <summary>
    /// Korutyna niszcząca zużytą sktrzynkę. Odtwarza dodatkowo dźwięk podnoszenia.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator MarkingAsUsed()
    {
        audioSource.Play();
        yield return new WaitForSeconds(pickupSound.length);
        Destroy(gameObject);
    }

}
