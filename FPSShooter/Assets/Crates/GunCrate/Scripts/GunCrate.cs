﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa skrzynki z bronią/amunicją.
/// </summary>
public class GunCrate : SupplyCrate
{
    /// <summary>
    /// Przenoszona broń.
    /// </summary>
    public GameObject weaponPrefab;

    /// <summary>
    /// Inicjalizacja. UStawienie ikony broni na skrzynce.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        GetComponentInChildren<SpriteRenderer>().sprite = weaponPrefab.GetComponent<Weapon>().Icon;
    }

    /// <summary>
    /// Dodanie graczowi broni lub amunicji, jeśli to możliwe.
    /// </summary>
    /// <param name="player">Podnoszący gracz.</param>
    public override void Resupply(Player player)
    {
        if (player.AddWeapon(weaponPrefab))
        {
            MarkAsUsed();
        }
    }

}
