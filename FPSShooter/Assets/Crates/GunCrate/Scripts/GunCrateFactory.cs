﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fabryka skrzynek z bronią.
/// </summary>
public class GunCrateFactory : CrateFactory
{
    /// <summary>
    /// Prefabrykat skrzynki.
    /// </summary>
    public GunCrate cratePrefab;
    /// <summary>
    /// Możliwe bronie pojawiające się w skrzynce.
    /// </summary>
    public List<GameObject> possibleWeapons;

    /// <summary>
    /// Metoda tworząca skrzynkę z losową bronią.
    /// </summary>
    /// <returns>Utworzona skrzynka.</returns>
    public override SupplyCrate CreateCrate()
    {
        cratePrefab.weaponPrefab = GetRandomWeapon();
        return Instantiate(cratePrefab);
    }

    /// <summary>
    /// Wybranie losowej broni dostępnej z wybranych.
    /// </summary>
    /// <returns>Losowo wybrana broń.</returns>
    private GameObject GetRandomWeapon()
    {
        return possibleWeapons[Random.Range(0, possibleWeapons.Count)];
    }
}
