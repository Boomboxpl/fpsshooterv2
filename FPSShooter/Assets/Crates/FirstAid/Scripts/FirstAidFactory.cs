﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fabryka apteczek.
/// </summary>
public class FirstAidFactory : CrateFactory
{
    /// <summary>
    /// Prefabrykat apteczki.
    /// </summary>
    public FirstAid cratePrefab;
    /// <summary>
    /// Rozkład dostępnych wartości przywracania życia.
    /// </summary>
    public AnimationCurve lifeDistribution;

    /// <summary>
    /// Metoda tworząca skrzynkę. Przypisuje jej poziom przywracanego życia zgodnie z rozkładem na podstawie liczby losowej od [0,1).
    /// </summary>
    /// <returns>Utworzona apteczka.</returns>
    public override SupplyCrate CreateCrate()
    {
        var crate = Instantiate(cratePrefab);
        crate.healthSupply = lifeDistribution.Evaluate(Random.Range(0, 1));
        return crate;
    }
}
