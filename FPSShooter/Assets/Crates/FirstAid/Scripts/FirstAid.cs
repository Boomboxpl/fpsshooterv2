﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa apteczki.
/// </summary>
public class FirstAid : SupplyCrate
{
    /// <summary>
    /// Ilość przywracanego życia.
    /// </summary>
    public float healthSupply = 20;

    /// <summary>
    /// Przywrócenie podnoszącemu graczowi życia, jeśli to możliwe.
    /// </summary>
    /// <param name="player">Podnoszący gracz.</param>
    public override void Resupply(Player player)
    {
        if (player.Heal(healthSupply))
        {
            MarkAsUsed();
        }
    }
}
