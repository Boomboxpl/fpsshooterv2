﻿using Extenstions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa karabinu automatycznego.
/// </summary>
public class Rifle : BulletWeapon
{
    /// <summary>
    /// Czas pomiędzy strzałami.
    /// </summary>
    [Header("Automatic Shoot Parameters")]
    public float lockTime;

    /// <summary>
    /// Animacja przeładowania.
    /// </summary>
    private Animation reloadAnimation;

    /// <summary>
    /// Czas od ostatniego strzału.
    /// </summary>
    private float lockElapsedTime;

    /// <summary>
    /// Czas przeładowania.
    /// </summary>
    public override float ReloadTime
    {
        get
        {
            if (reloadAnimation == null)
            {
                return 2.0f;
            }
            return reloadAnimation.clip.length;
        }
    }

    /// <summary>
    /// Broń gotowa do przeładowania.
    /// </summary>
    public override bool WeaponReadyForShoot => base.WeaponReadyForShoot && lockElapsedTime > lockTime;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        reloadAnimation = GetComponent<Animation>();
    }

    /// <summary>
    /// Aktualizacja w każdej klatce.
    /// </summary>
    public override void Update()
    {
        base.Update();
        lockElapsedTime += Time.deltaTime;
    }

    /// <summary>
    /// Przeładowanie. Względem BulletWeapon odtawrza dodatkową animację.
    /// </summary>
    /// <returns>True, gdy udało się przeładować.</returns>
    public override bool Reload()
    {
        if (base.Reload()) 
        {
            reloadAnimation.Play();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Wykonanie strzału. Aktualizuje czas ostatniego strzału.
    /// </summary>
    /// <param name="from">Miejsce rozpoczęcia strzału.</param>
    /// <param name="direction">Kierunek strzału.</param>
    /// <returns>True, gdy udało się strzelić.</returns>
    public override bool Shoot(Vector3 from, Vector3 direction)
    {  
        if(base.Shoot(from, direction))
        {
            lockElapsedTime = 0;
        }
        return false;
    }

    /// <summary>
    /// Funkcja wywoływana po pojawieniu się broni.
    /// </summary>
    public override void OnShow()
    {
        base.OnShow();
        reloadAnimation.Reset();
    }
}
