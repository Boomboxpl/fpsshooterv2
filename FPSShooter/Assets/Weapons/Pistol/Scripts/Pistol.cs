﻿using Extenstions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa pistoletu.
/// </summary>
public class Pistol : BulletWeapon
{
    /// <summary>
    /// Animacja przeładowania.
    /// </summary>
    private Animation reloadAnimation;

    /// <summary>
    /// Czas przeładowania.
    /// </summary>
    public override float ReloadTime { 
        get { 
            if(reloadAnimation == null)
            {
                return 4.0f;
            }
            return reloadAnimation.clip.length; 
        } 
    }

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        reloadAnimation = GetComponent<Animation>();
    }

    /// <summary>
    /// Przeładowanie. Względem BulletWeapon odtawrza dodatkową animację.
    /// </summary>
    /// <returns>True, gdy udało się przeładować.</returns>
    public override bool Reload()
    {
        if (base.Reload())
        {
            reloadAnimation.Play();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Funkcja wywoływana po pojawieniu się broni. Restart animacji
    /// </summary>
    public override void OnShow()
    {
        base.OnShow();
        reloadAnimation.Reset();
    }

    /// <summary>
    /// Funkcja wywoływana po ukryciu broni.
    /// </summary>
    public override void OnHide()
    {
        base.OnHide();
    }
}
