﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Broń korzystająca z kul. Strzały śledzone za pomocą promieni.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public abstract class BulletWeapon : MonoBehaviour, Weapon, RecoilingWeapon
{
    /// <summary>
    /// Krzywa kąta obrotu względem odrzutu.
    /// </summary>
    [Header("Attack parameters")]
    public AnimationCurve recoilCurve;
    /// <summary>
    /// Krzywa kąta rozrzutu wokół osi X względem odrzutu.
    /// </summary>
    public AnimationCurve aimDispersionCurveX;
    /// <summary>
    /// Krzywa kąta rozrzutu wokół osi Y względem odrzutu.
    /// </summary>
    public AnimationCurve aimDispersionCurveY;
    /// <summary>
    /// Krzywa skali obrazu celowania względem odrzutu.
    /// </summary>
    public AnimationCurve scaleAimImageToRecoil;
    /// <summary>
    /// Czy broń jest półautomatyczna.
    /// </summary>
    public bool isSemiAuto = false;
    /// <summary>
    /// Czy broń półautomatyczna miała puszczony spust.
    /// </summary>
    protected bool wasReleased = true;
    /// <summary>
    /// Szybkość powrotu do stanu bez odrzutu.
    /// </summary>
    [SerializeField]
    private float recoilReturnSpeed = 1;
    /// <summary>
    /// Aktualna wartość odrzutu.
    /// </summary>
    private float recoilMagnitude = 0;
    /// <summary>
    /// Punkt, z którego wychodzi strzał.
    /// </summary>
    [SerializeField]
    private Transform shootingPoint;
    /// <summary>
    /// Naturalny obrót broni.
    /// </summary>
    private Quaternion naturalRotation;

    /// <summary>
    /// Maksymalny dystans ataku.
    /// </summary>
    [SerializeField]
    private float maxDistance = 100;
    /// <summary>
    /// Maksymalna ilość kul w magazynku.
    /// </summary>
    [SerializeField]
    private int maxMagazine = 10;
    /// <summary>
    /// Maksymalny zapas kul.
    /// </summary>
    [SerializeField]
    private int maxBullets = 300;
    /// <summary>
    /// Aktualny stan magazynku.
    /// </summary>
    private int magazine;
    /// <summary>
    /// Aktualny stan zapasu.
    /// </summary>
    private int bullets;
    /// <summary>
    /// Pole informujące czy broń jest w trakcjie przeładowania.
    /// </summary>
    private bool reloading;
    /// <summary>
    /// Czas pozostały do przeładowania.
    /// </summary>
    private float leftToReload;
    /// <summary>
    /// Ilość zadawanych obrażeń.
    /// </summary>
    [SerializeField]
    private float damage = 2;

    /// <summary>
    /// Prefabrykat służący do rednerowania śladu.
    /// </summary>
    [Header("Prefabs")]
    [SerializeField]
    private GameObject linePrefab;
    /// <summary>
    /// Celownik podczas ładowania.
    /// </summary>
    [SerializeField]
    private Sprite reload;
    /// <summary>
    /// Celownik.
    /// </summary>
    [SerializeField]
    private Sprite aim;
    /// <summary>
    /// Ikona broni.
    /// </summary>
    [SerializeField]
    private Sprite icon;
    /// <summary>
    /// Dźwięk strzału.
    /// </summary>
    [SerializeField]
    private AudioClip shootSound;
    /// <summary>
    /// Dźwięk przeładowania.
    /// </summary>
    [SerializeField]
    private AudioClip reloadSound;

    /// <summary>
    /// Komponent odtwarzający dźwięk.
    /// </summary>
    private AudioSource audioSource;

    /// <summary>
    /// Maska warstw, do których można strzelać.
    /// </summary>
    private int layerMaskValue;

    /// <summary>
    /// Czas przeładowania.
    /// </summary>
    public abstract float ReloadTime { get; }

    /// <summary>
    /// Ilość kul w magazynku.
    /// </summary>
    virtual public int Magazine { get => magazine; set => magazine = value; }

    /// <summary>
    /// Ilość kul w zapasie.
    /// </summary>
    virtual public int BulletsLeft { get => bullets; set => bullets = value; }

    /// <summary>
    /// Ilość zadawanych obrażeń.
    /// </summary>
    virtual public float Damage { get => damage; set => damage = value; }

    /// <summary>
    /// Czy broń jest gotowa do strzału.
    /// </summary>
    virtual public bool IsReady => !reloading;

    /// <summary>
    /// Ikona reprezentująca broń.
    /// </summary>
    virtual public Sprite Icon => icon;

    /// <summary>
    /// Wartość odrzutu.
    /// </summary>
    virtual public float RecoilMagnitude { get => recoilMagnitude; set => recoilMagnitude = Mathf.Clamp01(value); }

    /// <summary>
    /// Broń gotowa do przeładowania.
    /// </summary>
    virtual public bool WeaponReadyForReload { get => !reloading && bullets > 0; }

    /// <summary>
    /// Można strzelić, jeśli tryb półautomatyczny.
    /// </summary>
    virtual public bool CanShootOnSemiAuto { get => isSemiAuto ? wasReleased : true; }

    /// <summary>
    /// Broń gotowa do strzału.
    /// </summary>
    virtual public bool WeaponReadyForShoot { get => true; }

    /// <summary>
    /// Funkcja edytora, pozwalająca na łatwe ustawienie zasięgu ataku.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position with max shoot distance
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, maxDistance);
    }

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    virtual public void Awake()
    { 
        if (linePrefab == null)
        {
            Debug.LogError("No line renderer in weapon");
        }

        naturalRotation = transform.localRotation;
        magazine = maxMagazine;
        bullets = maxBullets;
        reloading = false;
        audioSource = GetComponent<AudioSource>();
        LayerMask mask = LayerMask.GetMask("Character");
        layerMaskValue = mask.value;
        layerMaskValue = ~layerMaskValue;
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Aktualizuje wartość odrzutu i przeładowania.
    /// </summary>
    virtual public void Update()
    {
        if (reloading)
        {
            UpdateReload();
        }
        else
        {
            leftToReload = ReloadTime;
        }

        AdjustRecoil();
    }

    /// <summary>
    /// Przeładowanie
    /// </summary>
    /// <returns>True, gdy udało się przeładować.</returns>
    virtual public bool Reload()
    {
        if (WeaponReadyForReload)
        {
            audioSource.clip = reloadSound;
            audioSource.Play();

            InGameUIController.Instance.aimUI.SetImage(reload);
            reloading = true;
            leftToReload = ReloadTime;

            recoilMagnitude = 0;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Wykonanie strzału
    /// </summary>
    /// <param name="from">Miejsce rozpoczęcia strzału.</param>
    /// <param name="direction">Kierunek strzału.</param>
    /// <returns>True, gdy udało się strzelić.</returns>
    virtual public bool Shoot(Vector3 from, Vector3 direction)
    {
        //Checking if can shoot.
        if ((!WeaponReadyForReload && magazine <= 0) || !CanShootOnSemiAuto || !WeaponReadyForShoot)
        {
            return false; 
        }

        if (TryReloadIfEmpty())
        {
            return false;
        }

        magazine--;
        wasReleased = false;

        Vector3 hitPoint = SingleShoot(from, direction);
        DrawTrace(hitPoint);

        recoilMagnitude = 1;

        return true;
    }

    /// <summary>
    /// Zwolnienie spustu.
    /// </summary>
    public void FreeTrigger()
    {
        UpdateSemiAutoMechanism();
    }

    /// <summary>
    /// Odtworzenie zapasu amunicji.
    /// </summary>
    /// <returns>True, gdy brakowało amunicji i udało się go odtworzyć.</returns>
    public bool ResupplyBullets()
    {
        if(bullets == maxBullets && magazine == maxMagazine)
        {
            return false;
        }
        bullets = maxBullets;
        magazine = maxMagazine;
        return true;
    }

    /// <summary>
    /// Funkcja wywoływana po pojawieniu się broni.
    /// </summary>
    virtual public void OnShow()
    {
        audioSource.Stop();
        InGameUIController.Instance.aimUI.SetImage(aim);
    }

    /// <summary>
    /// Funkcja wywoływana po ukryciu broni. Zatrzymanie przeładowania.
    /// </summary>
    virtual public void OnHide()
    {
        //Stop reloading.
        reloading = false;
    }

    /// <summary>
    /// Wykonanie pojedynczego stzału.
    /// </summary>
    /// <param name="from">Miejsce rozpoczęcia strzału.</param>
    /// <param name="direction">Kierunek strzału.</param>
    /// <returns>True, gdy udało się strzelić.</returns>
    virtual protected Vector3 SingleShoot(Vector3 from, Vector3 direction)
    {
        audioSource.clip = shootSound;
        audioSource.Play();

        RaycastHit hit;
        Vector3 hitPoint;
        direction += GetDispersionVector();

        if (Physics.Raycast(from, direction, out hit, maxDistance, layerMaskValue))
        {
            hitPoint = hit.point;
            var target = hit.collider.gameObject.GetComponent<DestroyAble>();
            if (target != null)
            {
                target.TakeDamage(damage, hitPoint);
            }
        }
        else
        {
            //There was no hit, just draw trace.
            hitPoint = from + direction * maxDistance;
        }

        return hitPoint;
    }

    /// <summary>
    /// Rysowanie śladu kuli.
    /// </summary>
    /// <param name="hitPoint">Miejsce trafienia.</param>
    virtual protected void DrawTrace(Vector3 hitPoint)
    {
        var traceObject = Instantiate(linePrefab);
        var trace = traceObject.GetComponent<ShootTrace>();
        trace.DrawLine(shootingPoint.position, hitPoint);
    }

    /// <summary>
    /// Przeładowanie, jeśli można.
    /// </summary>
    /// <returns>True, gdy zaczęto przeładowanie.</returns>
    protected bool TryReloadIfEmpty()
    {
        if (magazine <= 0 && bullets > 0)
        {
            Reload();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Aktualizacja odrzutu.
    /// </summary>
    private void AdjustRecoil()
    {
        transform.localRotation = naturalRotation;
        transform.Rotate(Vector3.forward, recoilCurve.Evaluate(recoilMagnitude), Space.Self);
        recoilMagnitude -= recoilReturnSpeed * Time.deltaTime;
        recoilMagnitude = Mathf.Clamp01(recoilMagnitude);
        InGameUIController.Instance.aimUI.SetImageScale(scaleAimImageToRecoil.Evaluate(recoilMagnitude));
    }

    /// <summary>
    /// Generowanie wektora rozrzutu.
    /// </summary>
    /// <returns>Wektor z losowym rozrzutem, zależnym od wartości odrzutu.</returns>
    private Vector3 GetDispersionVector()
    {
        var result = new Vector3();
        var xValue = aimDispersionCurveX.Evaluate(recoilMagnitude);
        result.x = Random.Range(-xValue, xValue);
        result.y = Random.Range(0, aimDispersionCurveY.Evaluate(recoilMagnitude));
        result.z = 0;
        return result;
    }

    /// <summary>
    /// Aktualizacja przeładowania.
    /// </summary>
    private void UpdateReload()
    {
        leftToReload -= Time.deltaTime;
        if (leftToReload < 0)
        {
            reloading = false;
            InGameUIController.Instance.aimUI.SetImage(aim);
            if (bullets - maxMagazine < 0)
            {
                magazine = bullets;
                bullets = 0;
            }
            else
            {
                bullets -= maxMagazine - magazine;
                magazine = maxMagazine;
            }
            InGameUIController.Instance.SetMagazineAndBullets(Magazine, bullets);
        }
    }

    /// <summary>
    /// Aktualizacja mechanizmu półatomatycznego.
    /// </summary>
    private void UpdateSemiAutoMechanism()
    {
        if (isSemiAuto)
        {
            wasReleased = true;
        } 
    }
}
