﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interfejs dla broni.
/// </summary>
public interface Weapon
{
    /// <summary>
    /// Ilość kul w magazynku.
    /// </summary>
    int Magazine { get; set; }

    /// <summary>
    /// Ilość kul w zapasie.
    /// </summary>
    int BulletsLeft { get; set; }

    /// <summary>
    /// Ilość zadawanych obrażeń.
    /// </summary>
    float Damage { get; set; }

    /// <summary>
    /// Czy broń jest gotowa do strzału.
    /// </summary>
    bool IsReady { get; }

    /// <summary>
    /// Ikona reprezentująca broń.
    /// </summary>
    Sprite Icon { get; }

    /// <summary>
    /// Wykonanie strzału.
    /// </summary>
    /// <param name="from">Miejsce rozpoczęcia strzału.</param>
    /// <param name="direction">Kierunek strzału.</param>
    /// <returns>True, gdy udało się strzelić.</returns>
    bool Shoot(Vector3 from, Vector3 direction);

    /// <summary>
    /// Zwolnienie spustu.
    /// </summary>
    void FreeTrigger();

    /// <summary>
    /// Przeładowanie
    /// </summary>
    /// <returns>True, gdy udało się przeładować.</returns>
    bool Reload();

    /// <summary>
    /// Odtworzenie zapasu amunicji.
    /// </summary>
    /// <returns>True, gdy brakowało amunicji i udało się go odtworzyć.</returns>
    bool ResupplyBullets();

    /// <summary>
    /// Funkcja wywoływana po pojawieniu się broni.
    /// </summary>
    void OnShow();

    /// <summary>
    /// Funkcja wywoływana po ukryciu broni.
    /// </summary>
    void OnHide();
}
