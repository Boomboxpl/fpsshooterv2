﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Broń z odrzutem.
/// </summary>
public interface RecoilingWeapon
{
    /// <summary>
    /// Wartość odrzutu.
    /// </summary>
    float RecoilMagnitude { get; set; }
}
