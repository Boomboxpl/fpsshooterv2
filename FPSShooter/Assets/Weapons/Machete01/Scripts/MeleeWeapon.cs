﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa broni białej.
/// </summary>
public class MeleeWeapon : MonoBehaviour, Weapon
{
    /// <summary>
    /// Zadawane obrażenia.
    /// </summary>
    [SerializeField]
    private float damage = float.MaxValue;
    /// <summary>
    /// Kąt machnięcia bronią.
    /// </summary>
    [SerializeField]
    private float maxSwingAngle = 90f;
    /// <summary>
    /// Czas machnięcia.
    /// </summary>
    [SerializeField]
    private float swingTime = 0.5f;

    /// <summary>
    /// Celownik.
    /// </summary>
    [SerializeField]
    private Sprite aim;
    /// <summary>
    /// Ikona vroni.
    /// </summary>
    [SerializeField]
    private Sprite icon;

    /// <summary>
    /// Informacja czy broń jest w trakcie animacji.
    /// </summary>
    private bool animating = false;
    /// <summary>
    /// Informacja czy przycisk ataku został puszczony.
    /// </summary>
    private bool wasReleased = true;
    /// <summary>
    /// Informacja czy wystąpiła kombinacja ataku.
    /// </summary>
    private bool combination = false;
    /// <summary>
    /// Komponent wykrywania kolizji.
    /// </summary>
    private Collider collider;

    /// <summary>
    /// Magazynek, zawsze równy 0.
    /// </summary>
    virtual public int Magazine { get => 0; set {} }

    /// <summary>
    /// Pozostała ilość kul, zawsze równa 0.
    /// </summary>
    virtual public int BulletsLeft { get => 0; set { } }

    /// <summary>
    /// Ilość zadawanych obrażeń.
    /// </summary>
    virtual public float Damage { get => damage; set => damage = value; }

    /// <summary>
    /// Czy broń jest gotowa do ataku.
    /// </summary>
    virtual public bool IsReady => !animating;

    /// <summary>
    /// Ikona reprezentująca broń.
    /// </summary>
    virtual public Sprite Icon => icon;

    /// <summary>
    /// Funckja wywoływana podczas aktywowania obiektu. Ustawia wartości tak by nie było wykonywanego ataku.
    /// </summary>
    public void OnEnable()
    {
        animating = false;
        wasReleased = true;
        combination = false;
        collider.enabled = false;
    }

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        collider = GetComponent<Collider>();
        collider.enabled = false;
    }

    /// <summary>
    /// Wystąpienie kolizji z innym obiektem. Zadaje mu obrażenia, jeżeli to możliwe.
    /// </summary>
    /// <param name="collision">Informacje o kolizji.</param>
    public void OnCollisionEnter(Collision collision)
    {
        var target = collision.gameObject.GetComponent<DestroyAble>();
        if(target != null)
        {
            target.TakeDamage(Damage, collision.collider.ClosestPoint(transform.position));
        }
    }

    /// <summary>
    /// Przeładowanie. Akcja ignorowana.
    /// </summary>
    /// <returns>False</returns>
    virtual public bool Reload()
    {
        return false;
    }

    /// <summary>
    /// Odtworzenie zapasu amunicji. Akcja ignorowana.
    /// </summary>
    /// <returns>False</returns>
    virtual public bool ResupplyBullets()
    {
        return false;
    }

    /// <summary>
    /// Wykonanie ataku. Ignoruje parametry, rozpoczyna animację ataku.
    /// </summary>
    /// <param name="from">Miejsce rozpoczęcia ataku.</param>
    /// <param name="direction">Kierunek ataku.</param>
    /// <returns>True.</returns>
    virtual public bool Shoot(Vector3 from, Vector3 direction)
    {

        if (!animating && wasReleased) {
            animating = true;
            wasReleased = false;
            combination = false;
            StartCoroutine(AnimateSwing());
        }
        else if(animating && wasReleased)
        {
            combination = true;
        }
        return true;
    }

    /// <summary>
    /// Zwolnienie spustu.
    /// </summary>
    virtual public void FreeTrigger()
    {
        wasReleased = true;
    }

    /// <summary>
    /// Funkcja wywoływana po pojawieniu się broni.
    /// </summary>
    virtual public void OnShow()
    {
        InGameUIController.Instance.aimUI.SetImage(aim);
        collider.enabled = false;
    }

    /// <summary>
    /// Funkcja wywoływana po ukryciu broni.
    /// </summary>
    virtual public void OnHide()
    {

    }

    #region Animations
    /// <summary>
    /// Animacja ataku. Włącza możliwość kolizji dla broni. Po wykonaniu jednego machnięcia, wykonuje kolejne,
    /// jeśli wystąpiła kombinacja. Na koniec powraca do punktu początkowego i wyłącza kolizję.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    virtual protected IEnumerator AnimateSwing()
    {
        //TODO:Turn on/off colisions.
        collider.enabled = true;
        var startPosition = transform.localPosition;
        var startRotation = transform.localRotation;
        //Start
        {
            float angle = 90;
            float time = swingTime / 5;
            float actualAngle = 0;
            while (Mathf.Abs(actualAngle) < Mathf.Abs(angle))
            {
                float angleDelta = angle * Time.deltaTime / time;
                actualAngle += angleDelta;
                transform.Rotate(Vector3.up, angleDelta);

                yield return null;
            }
        }
        //Slash
        {

            float angle = -maxSwingAngle;
            float time = 4 * swingTime / 5;
            float actualAngle = 0;
            while (Mathf.Abs(actualAngle) < Mathf.Abs(angle))
            {
                float angleDelta = angle * Time.deltaTime / time;
                actualAngle += angleDelta;
                transform.RotateAround(transform.parent.position, transform.parent.up, angleDelta);

                yield return null;
            }

        }
        //If combination - return with hit.
        if (combination)
        {
            combination = false;
            {

                float angleX = 150;
                float angleY = -50;
                float time = swingTime / 5;
                float actualAngle = 0;
                while (Mathf.Abs(actualAngle) < Mathf.Abs(angleX))
                {
                    float angleDelta = angleX * Time.deltaTime / time;
                    actualAngle += angleDelta;

                    transform.Rotate(Vector3.right, angleDelta);
                    transform.Rotate(transform.parent.up, angleY * Time.deltaTime / time);
                    yield return null;
                }
            }

            {
                float angle = maxSwingAngle;
                float time = 4 * swingTime / 5;
                float actualAngle = 0;
                while (Mathf.Abs(actualAngle) < Mathf.Abs(angle))
                {
                    float angleDelta = angle * Time.deltaTime / time;
                    actualAngle += angleDelta;

                    transform.RotateAround(transform.parent.position, transform.parent.up, angleDelta);
                    yield return null;
                }
            }
        }

        //Return
        var endPosition = transform.localPosition;
        var endRotation = transform.localRotation;
        {
            int steps = 20;
            float time = 0.1f;
            float elapsedTime = 0;
            while (elapsedTime < time)
            {
                elapsedTime += Time.deltaTime;
                transform.localPosition = Vector3.Lerp(endPosition, startPosition, elapsedTime/time);
                transform.localRotation = Quaternion.Slerp(endRotation, startRotation, elapsedTime/time);
                yield return new WaitForSeconds(time / steps);
            }
            transform.localPosition = startPosition;
            transform.localRotation = startRotation;
        }

        animating = false;
        transform.localRotation = startRotation;
        transform.localPosition = startPosition;
        collider.enabled = false;
    }

    #endregion
}
