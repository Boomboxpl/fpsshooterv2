﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa obiektów pokazujących ślad po ruchu kuli.
/// </summary>
public class ShootTrace : MonoBehaviour
{
    /// <summary>
    /// Czas zanikania.
    /// </summary>
    public float fadingTime;
    /// <summary>
    /// Komponent renderujący linię.
    /// </summary>
    private LineRenderer renderer;
    
    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    void Awake()
    {
        renderer = GetComponent<LineRenderer>();
    }

    /// <summary>
    /// Rysowanie linii.
    /// </summary>
    /// <param name="p1">Pierwszy punkt</param>
    /// <param name="p2">Drugi punkt</param>
    public void DrawLine(Vector3 p1, Vector3 p2)
    {
        renderer.SetPosition(0, p1);
        renderer.SetPosition(1, p2);
        StartCoroutine(Fading());
    }

    /// <summary>
    /// Korutyna zanikania koloru.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator Fading()
    {
        float remainingTime = fadingTime;
        Color startColor = renderer.startColor;
        Color endColor = renderer.endColor;
        while (remainingTime > 0f)
        {
            renderer.startColor = new Color(startColor.r, startColor.g, startColor.b, startColor.a * (remainingTime / fadingTime));
            renderer.endColor = new Color(endColor.r, endColor.g, endColor.b, endColor.a * (remainingTime / fadingTime));
            remainingTime -= Time.deltaTime;
            yield return null;
        }
        
        Destroy(gameObject);
    }
}
