﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa strzelby.
/// </summary>
public class Shotgun : BulletWeapon
{
    /// <summary>
    /// Ilość kul z jedengo wystrzału.
    /// </summary>
    [Header("Attack Parameters")]
    public float shootCount;
    /// <summary>
    /// Czas pomiędzy strzałami.
    /// </summary>
    public float shootTime;
    /// <summary>
    /// Kąt rozrzutu kul z jednego strzału.
    /// </summary>
    public float maxSprayAngle;

    /// <summary>
    /// Parametr Animatora.
    /// </summary>
    [Header("Animator Mapping")]
    public string shootParameter = "Shoot";
    /// <summary>
    /// Parametr Animatora.
    /// </summary>
    public string reloadParameter = "Reload";
    /// <summary>
    /// Parametr Animatora.
    /// </summary>
    public string idleParameter = "Idle";

    /// <summary>
    /// ID odpowiedniego parametru Animatora.
    /// </summary>
    protected int shootID;
    /// <summary>
    /// ID odpowiedniego parametru Animatora.
    /// </summary>
    protected int reloadID;
    /// <summary>
    /// ID odpowiedniego parametru Animatora.
    /// </summary>
    protected int idleID;

    /// <summary>
    /// Komponent animatora.
    /// </summary>
    private Animator animator;
    /// <summary>
    /// Czas od ostatniego strzału.
    /// </summary>
    private float shootElapsedTime;

    /// <summary>
    /// Czas przeładowania.
    /// </summary>
    public override float ReloadTime => 1f;

    /// <summary>
    /// Broń gotowa do strzału.
    /// </summary>
    public override bool WeaponReadyForShoot => base.WeaponReadyForShoot && shootElapsedTime > shootTime;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        animator = GetComponentInChildren<Animator>(true);
        shootID = Animator.StringToHash(shootParameter);
        reloadID = Animator.StringToHash(reloadParameter);
        idleID = Animator.StringToHash(idleParameter);
        shootElapsedTime = shootTime;
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Aktualizuje czas od ostatniego strzału.
    /// </summary>
    public override void Update()
    {
        base.Update();
        shootElapsedTime += Time.deltaTime;
    }

    /// <summary>
    /// Przeładowanie
    /// </summary>
    /// <returns>True, gdy udało się przeładować.</returns>
    public override bool Reload()
    {
        if (base.Reload())
        {
            animator.SetTrigger(reloadID);
            shootElapsedTime = 0;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Wykonanie strzału z rozrzutem wielu pocisków.
    /// </summary>
    /// <param name="from">Miejsce rozpoczęcia strzału.</param>
    /// <param name="direction">Kierunek strzału.</param>
    /// <returns>True, gdy udało się strzelić.</returns>
    public override bool Shoot(Vector3 from, Vector3 direction)
    {
        //Checking if can shoot.
        if ((!WeaponReadyForReload && Magazine <= 0) || !CanShootOnSemiAuto || !WeaponReadyForShoot)
        {
            return false;
        }

        if (TryReloadIfEmpty())
        {
            return false;
        }

        Magazine--;
        wasReleased = false;

        for(int i = 0; i < shootCount; i++)
        {
            var randomizedDirection = RandomizeDirection(direction);
            Vector3 hitPoint = SingleShoot(from, randomizedDirection);
            DrawTrace(hitPoint);
        }

        animator.SetTrigger(shootID);
        shootElapsedTime = 0;
        RecoilMagnitude = 1;

        return true;
    }

    /// <summary>
    /// Funkcja wywoływana po pojawieniu się broni. Ustawia stan animatora na Idle.
    /// </summary>
    public override void OnShow()
    {
        base.OnShow();
        animator.SetTrigger(idleID);
    }

    /// <summary>
    /// Generowanie losowego wektoru uwzględniając kierunek strzału orza maksymalny kąt.
    /// </summary>
    /// <param name="direction">Kierunek strzału.</param>
    /// <returns>Losowy wektor.</returns>
    protected Vector3 RandomizeDirection(Vector3 direction)
    {
        var randomX = Random.Range(-maxSprayAngle, maxSprayAngle);
        var randomY = Random.Range(-maxSprayAngle, maxSprayAngle);
        return Quaternion.Euler(randomX, randomY, 0) * direction;
    }

}
