﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Przestrzeń nazw z rozszerzeniami klas.
/// </summary>
namespace Extenstions
{
    /// <summary>
    /// Klasa definiująca rozszerzenia klasy Animation.
    /// </summary>
    public static partial class AnimationExtensions
    {
        /// <summary>
        /// Reset animacji. Powrót do pierwszej klatki.
        /// </summary>
        /// <param name="animation">Resetowana animacja.</param>
        public static void Reset(this Animation animation)
        {
            foreach(AnimationState state in animation)
            {
                state.enabled = true;
                state.weight = 1;
                state.normalizedTime = 0;

                animation.Sample();

                state.enabled = false;
            }
        }
    }
}

