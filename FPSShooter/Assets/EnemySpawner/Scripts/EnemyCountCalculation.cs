﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa bazowa wyliczająca ilość przeciwników w fali.
/// </summary>
public class EnemyCountCalculation : MonoBehaviour
{
    /// <summary>
    /// Metoda wyliczająca ilość przeciwników.
    /// </summary>
    /// <param name="wave">Aktualna fala.</param>
    /// <returns>Ilość przeciwników.</returns>
    virtual public int Calculate(int wave)
    {
        return wave;
    }
}
