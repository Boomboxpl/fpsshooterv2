﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa ustawiająca płaszczyznę tak, by zawsze była zwrócona w stronę gracza. 
/// Dodatkowo płaszczyzna będzie się obracać wokół osi Y.
/// </summary>
public class BilboardPortal : MonoBehaviour
{
    /// <summary>
    /// Szybkość obrotu wokół osi Y.
    /// </summary>
    public float rotationSpeed = 2f;

    /// <summary>
    /// Aktualny obrót.
    /// </summary>
    private float rotation = 0;

    /// <summary>
    /// Aktualizacja w każdej klatce. Zwrócenie w stronę gracza oraz obrót wokół własnej osi.
    /// </summary>
    void Update()
    {
        transform.rotation = Quaternion.identity;
        transform.LookAt(GameManager.Instance.mainCamera.transform.position);
        transform.Rotate(90, 0, 0);
        rotation += rotationSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, rotation, Space.Self);
    }
}
