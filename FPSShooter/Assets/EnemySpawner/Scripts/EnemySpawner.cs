﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca tworzenia przeciwników.
/// </summary>
public class EnemySpawner : MonoBehaviour
{
    /// <summary>
    /// Lista możliwych przeciwników.
    /// </summary>
    public List<GameObject> possibleEnemies;
    /// <summary>
    /// Czas pomiędzy pojedyńczymi wrogami wychodzącymi z jednego punktu. 
    /// </summary>
    public float spawnInterval;
    /// <summary>
    /// Komponent wyliczający ilość wrogów.
    /// </summary>
    public EnemyCountCalculation enemyCountCalculation;

    /// <summary>
    /// Referencja do gracza.
    /// </summary>
    private Player player;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    private void Awake()
    {
        if (enemyCountCalculation == null)
        {
            enemyCountCalculation = GetComponent<EnemyCountCalculation>();
        }
        player = FindObjectOfType<Player>();
    }

    /// <summary>
    /// Procedura tworzenia wrogów. Punkty pojawiania się wrogów, są określane na podstawie pozycji dzieci.
    /// </summary>
    /// <param name="wave">Numer fali.</param>
    /// <returns>Ilość utworzonych wrogów.</returns>
    public int SpawnEnemies(int wave)
    {
        int enemiesToSpawn = enemyCountCalculation.Calculate(wave);
        int enemiesRemain = enemiesToSpawn;
        List<SpawnPointWeight> spawnPoints;
        GetSpawnPointWeights(out spawnPoints);
        for (int i = 0; i< spawnPoints.Count; i++)
        {
            int spawnPointEnemiesCount = Mathf.RoundToInt(spawnPoints[i].weight * enemiesToSpawn);
            spawnPointEnemiesCount = System.Math.Min(spawnPointEnemiesCount, enemiesRemain);
            StartCoroutine(SpawnEnemiesOnPoint(spawnPoints[i].spawnPoint, spawnPointEnemiesCount));
            enemiesRemain -= spawnPointEnemiesCount;
        }
        StartCoroutine(SpawnEnemiesOnPoint(spawnPoints[spawnPoints.Count - 1].spawnPoint, enemiesRemain));
        return enemiesToSpawn;
    }

    /// <summary>
    /// Wyliczenie wag puntków pojawiania się wrogów. Waga jest proporcjonalna do odległości od gracza.
    /// </summary>
    /// <param name="spawnPoints">Zestaw punktów.</param>
    /// <returns>True, gdy udało się wyliczyć wagi.</returns>
    private bool GetSpawnPointWeights(out List<SpawnPointWeight> spawnPoints)
    {
        if(transform.childCount == 0)
        {
            spawnPoints = null;
            return false;
        }

        spawnPoints = new List<SpawnPointWeight>(transform.childCount);
        foreach (Transform child in transform)
        {
            spawnPoints.Add(new SpawnPointWeight(child, DistanceToPlayer(child.position)));
        }
        spawnPoints.Sort((spawnPoint1, spawnPoint2) => spawnPoint1.weight.CompareTo(spawnPoint2.weight));
        NormalizeWeights(spawnPoints);
        return true;

    }

    /// <summary>
    /// Wyliczenie dystansu do gracza.
    /// </summary>
    /// <param name="spawnPointPosition">Punkt.</param>
    /// <returns>Odległość punktu od gracza.</returns>
    private float DistanceToPlayer(Vector3 spawnPointPosition)
    {
        return Vector3.Distance(player.transform.position, spawnPointPosition);
    }

    /// <summary>
    /// Normalizacja wag.
    /// </summary>
    /// <param name="spawnPoints">Zestaw punktów.</param>
    /// <returns>Suma wag przed normalizacją.</returns>
    private float NormalizeWeights(List<SpawnPointWeight> spawnPoints)
    {
        float sum = 0;
        foreach(var spawnPoint in spawnPoints)
        {
            sum += spawnPoint.weight;
        }

        for(int i=0; i< spawnPoints.Count; i++)
        {
            spawnPoints[i] = new SpawnPointWeight(spawnPoints[i].spawnPoint, spawnPoints[i].weight / sum);
        }

        return sum;
    }

    /// <summary>
    /// Korytna tworzenia wrogów w punkcie.
    /// </summary>
    /// <param name="spawnPoint">Punkt pojawiania się wrogów.</param>
    /// <param name="enemyCount">Ilość wrogów, która powinna się pojawić.</param>
    /// <returns></returns>
    private IEnumerator SpawnEnemiesOnPoint(Transform spawnPoint, int enemyCount)
    {
        for(int i = 0; i< enemyCount; i++)
        {
            Instantiate(GetRandomEnemy(), spawnPoint.position, Quaternion.identity);
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    /// <summary>
    /// Wybranie losowego wroga z dostępnych.
    /// </summary>
    /// <returns>Losowy wróg.</returns>
    private GameObject GetRandomEnemy()
    {
        return possibleEnemies[Random.Range(0, possibleEnemies.Count)];
    }

    /// <summary>
    /// Struktura przechowująca punkt oraz jego wagę.
    /// </summary>
    private struct SpawnPointWeight
    {
        /// <summary>
        /// Punkt.
        /// </summary>
        public Transform spawnPoint;
        /// <summary>
        /// Waga.
        /// </summary>
        public float weight;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="newSpawnPoint">Punkt</param>
        /// <param name="newWeight">Wagam</param>
        public SpawnPointWeight(Transform newSpawnPoint, float newWeight)
        {
            spawnPoint = newSpawnPoint;
            weight = newWeight;
        }
    }
}
