﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa bazowa wyliczająca ilość przeciwników w fali. Korzysta z wzoru x^power + offset.
/// </summary>
public class EnemyCountPower : EnemyCountCalculation
{
    /// <summary>
    /// Potęga do której zostanie podniesiony numer fali.
    /// </summary>
    public float power;
    /// <summary>
    /// Przesunięcie ilości przeciwników.
    /// </summary>
    public int offset;

    /// <summary>
    /// Metoda wyliczająca ilość przeciwników.
    /// </summary>
    /// <param name="wave">Aktualna fala.</param>
    /// <returns>Ilość przeciwników.</returns>
    public override int Calculate(int wave)
    {
        return (int)Mathf.Pow(wave, power) + offset;
    }
}
