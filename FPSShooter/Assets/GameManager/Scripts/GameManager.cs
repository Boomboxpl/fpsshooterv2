﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Zarządca gry. Przenosi informacje pomiędzy scenami gry i menu.
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Instancja wzorca singleton
    /// </summary>
    private static GameManager _instance;
    /// <summary>
    /// Getter dla wzorca singleton.
    /// </summary>
    public static GameManager Instance { get { return _instance; } }

    /// <summary>
    /// Numer fali.
    /// </summary>
    public int Wave { get; private set; }

    /// <summary>
    /// Czas w grze. Wyliczany w różny sposób podczas gry i po jej zakończeniu.
    /// </summary>
    public float ElapsedTime { 
        get 
        {
            if(state == State.Active)
            {
                return Time.time - startTime;
            }
            else
            {
                return endTime - startTime;
            }
        }
    }

    /// <summary>
    /// Stan zarządcy.
    /// </summary>
    public State state;
    /// <summary>
    /// Czas pomiędzy zrzutami skrzynek.
    /// </summary>
    public float crateDropInterval;
    /// <summary>
    /// Scena ładowana po zakończeniu gry.
    /// </summary>
    public MenuScenesManager.Scene endGameScene = MenuScenesManager.Scene.ScoreMenu;
    /// <summary>
    /// Główna kamera sceny.
    /// </summary>
    [HideInInspector]
    public Camera mainCamera;

    /// <summary>
    /// Czas rozpoczęcia gry.
    /// </summary>
    private float startTime;
    /// <summary>
    /// Czas zakończenia gry.
    /// </summary>
    private float endTime;

    /// <summary>
    /// Zarządca tworzenia przeciwników.
    /// </summary>
    private EnemySpawner enemySpawner;
    /// <summary>
    /// Zarządca tworzenia skrzynek.
    /// </summary>
    private CrateSpawner crateSpawner;
    /// <summary>
    /// Uchwyt korutyny tworzącej skrzynki.
    /// </summary>
    private Coroutine crateSpawnCorotine;
    /// <summary>
    /// Liczba wrogów w aktualnej fali.
    /// </summary>
    private int waveEnemiesCount = 0;

    /// <summary>
    /// Komponent odpowiedzialny za muzykę w grze.
    /// </summary>
    private MusicBackground music;

    /// <summary>
    /// Wywoływane po załadowaniu sceny. Znajduje główną kamerę.
    /// </summary>
    /// <param name="scene">Scena. Parametr ignorowany.</param>
    /// <param name="mode">Tryb ładowania sceny. Parametr ignorowany.</param>
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        mainCamera = Camera.main;
    }

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
            music = GetComponent<MusicBackground>();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
    }

    /// <summary>
    /// Rozpoczęcie. Odtworzenie muzyki.
    /// </summary>
    public void Start()
    {
        music.Play();
    }

    /// <summary>
    /// Powrót do wartości domyślnych wyniku.
    /// </summary>
    public void Reset()
    {
        Wave = 0;
        startTime = 0f;
        endTime = 0f;
    }

    /// <summary>
    /// Procedura rozpoczęcia gry. Jeśli gra już została rozpoczęta, nic nie zostanie wykonane.
    /// </summary>
    public void StartGame()
    {
        if (state == State.Passive)
        {
            Wave = 0;
            startTime = Time.time;
            endTime = 0f;
            state = State.Active;
            enemySpawner = FindObjectOfType<EnemySpawner>();
            crateSpawner = FindObjectOfType<CrateSpawner>();
            crateSpawnCorotine = StartCoroutine(CrateSpawnRoutine());
            StartNewWave();
        }
    }

    /// <summary>
    /// Procedura zakończenia gry. Jeżeli gra nie była rozpoczęta, nic nie zostanie wykonane.
    /// </summary>
    public void EndGame()
    {
        if (state == State.Active)
        {
            endTime = Time.time;
            state = State.Passive;
            if (crateSpawnCorotine != null) 
            {
                StopCoroutine(crateSpawnCorotine);
            }

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            MenuScenesManager.Instance.Load(endGameScene);
        }
    }

    /// <summary>
    /// Usunięcie przeciwnika z puli. Gdy był to ostatni przeciwnik, rozpoczyna kolejną falę.
    /// </summary>
    public void EnemyKilled()
    {
        if (state == State.Active)
        {
            waveEnemiesCount--;
            InGameUIController.Instance.enemyCountText.SetTextValue(waveEnemiesCount);
            if (waveEnemiesCount <= 0)
            {
                StartNewWave();
            }
        }
    }

    /// <summary>
    /// Rozpoczęcie nowej fali.
    /// </summary>
    private void StartNewWave()
    {
        if (state == State.Active)
        {
            Wave++;
            waveEnemiesCount = enemySpawner.SpawnEnemies(Wave);
            InGameUIController.Instance.enemyCountText.SetTextValue(waveEnemiesCount);
            InGameUIController.Instance.SetWave(Wave);
        }
    }

    /// <summary>
    /// Korutyna tworząca skrzynki w odpowiednich odstępach.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator CrateSpawnRoutine()
    {
        while (true)
        {
            if(state != State.Active)
            {
                yield break;
            }
            if (crateSpawner != null)
            {
                crateSpawner.SpawnCrates();
                yield return new WaitForSeconds(crateDropInterval);
            }
        }
    }

    /// <summary>
    /// Stan zarządcy.
    /// </summary>
    public enum State 
    { 
        /// <summary>
        /// Stan pasywny. Po zakończeniu lub przed rozpoczęciem gry.
        /// </summary>
        Passive, 
        /// <summary>
        /// Stan aktywny. Podczas zarządzania grą.
        /// </summary>
        Active
    }
}
