﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa komponentu odpowiedzialnego za muzykę w grze.
/// </summary>
public class MusicBackground : MonoBehaviour
{
    /// <summary>
    /// Dostępne ścieżki dźwiękowe.
    /// </summary>
    public List<AudioClip> clips;
    /// <summary>
    /// Komponent odtwarzający dźwięk.
    /// </summary>
    public AudioSource source;

    /// <summary>
    /// Stan działania muzyki.
    /// </summary>
    private State state;

    /// <summary>
    /// Inicjalizacja. Stan ustawiany na State.Stopped.
    /// </summary>
    private void Awake()
    {
        if (source == null) { 
            source = GetComponent<AudioSource>();
        }
        state = State.Stopped;
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Wybiera inną losową ścieżkę po zakończeniu poprzedniej ścieżki.
    /// </summary>
    private void Update()
    {
        if (state == State.Playing && !source.isPlaying)
        {
            PlayRandomClip();
        }
    }

    /// <summary>
    /// Odtwarzanie muzyki. Po pauzie wznowi muzykę. Po zatrzymaniu zacznie nowy utwór.
    /// </summary>
    public void Play()
    {
        if (state == State.Paused)
        {
            source.UnPause();
        }
        else if (state == State.Stopped)
        {
            PlayRandomClip();
        }
    }

    /// <summary>
    /// Pauzowanie muzyki.
    /// </summary>
    public void Pause()
    {
        state = State.Paused;
        source.Pause();
    }

    /// <summary>
    /// Zatrzymywanie muzyki.
    /// </summary>
    public void Stop()
    {
        state = State.Stopped;
        source.Stop();
    }

    /// <summary>
    /// Rozpoczęcie losowej ścieżki dźwiękowej, wybranej z dostępnych.
    /// </summary>
    private void PlayRandomClip()
    {
        source.clip = GetRandomClip();
        source.Play();
        state = State.Playing;
    }

    /// <summary>
    /// Wybranie losowej ścieżki dźwiękowej, wybranej z dostępnych.
    /// </summary>
    /// <returns>Losowa ścieżka dźwiękowa.</returns>
    private AudioClip GetRandomClip()
    {
        return clips[Random.Range(0, clips.Count)];
    }

    /// <summary>
    /// Stan działania muzyki.
    /// </summary>
    private enum State
    {
        /// <summary>
        /// W trakcie odtwarzania.
        /// </summary>
        Playing, 
        /// <summary>
        /// Odtwarzanie wstrzymane.
        /// </summary>
        Paused, 
        /// <summary>
        /// Odtwarzanie zatrzymane.
        /// </summary>
        Stopped
    }
}
