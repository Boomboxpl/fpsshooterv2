﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zarządca paska życia.
/// </summary>
public class HealthBar : MonoBehaviour
{
    /// <summary>
    /// Obraz, którego kolor będzie zmieniany wraz z spadkiem życia.
    /// </summary>
    public Image status;
    /// <summary>
    /// Gradien definiujący kolor paska życia, względem jego wypełnienia.
    /// </summary>
    public Gradient barGradient;

    /// <summary>
    /// Szybkość zmiany poziomu na pasku życia.
    /// </summary>
    public float changeSpeed = 0.1f;

    /// <summary>
    /// Suwak, który ustawia wypełnienie paska.
    /// </summary>
    private Slider fillingSlider;
    /// <summary>
    /// Stosunek poziomu życia do maksymalnego poziomu życia.
    /// </summary>
    private float healthRatio;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        fillingSlider = GetComponent<Slider>();
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Przesuwa pasek życia do osiągnięcia żądanej proporcji.
    /// </summary>
    public void Update()
    {
        if(fillingSlider.value < healthRatio)
        {
            fillingSlider.value += changeSpeed * Time.deltaTime;
            if(fillingSlider.value > healthRatio)
            {
                fillingSlider.value = healthRatio;
            }
        }
        else if(fillingSlider.value > healthRatio)
        {
            fillingSlider.value -= changeSpeed * Time.deltaTime;
            if (fillingSlider.value < healthRatio)
            {
                fillingSlider.value = healthRatio;
            }
        }

        status.color = barGradient.Evaluate(fillingSlider.value);
    }

    /// <summary>
    /// Ustawia aktualną proporjcę punktów życia do maksymalnych punktów życia.
    /// </summary>
    /// <param name="health">Liczba punktów życia.</param>
    /// <param name="maxHealth">Maksymalna liczba punktów życia.</param>
    public void SetHealth(float health, float maxHealth)
    {
        healthRatio = Mathf.Clamp(health, 0, maxHealth) / maxHealth;
    }

}
