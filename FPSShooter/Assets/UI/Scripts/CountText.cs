﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zarządca licznika na interfejsie użytkownika.
/// </summary>
public class CountText : MonoBehaviour
{
    /// <summary>
    /// Żądana liczba cyfr.
    /// </summary>
    public int NumbersCount = 3;

    /// <summary>
    /// Tekst, na którym będzie wyświetlana wartość licznika.
    /// </summary>
    protected TextMeshProUGUI textUI;
    /// <summary>
    /// Ostatnia wartość.
    /// </summary>
    protected int lastValue = int.MaxValue;

    /// <summary>
    /// Maksymalna wartość. Wyliczana jako 10 do potęgi NumberCount.
    /// </summary>
    private int maxValue;
    /// <summary>
    /// Format wypisywania wartości licznika.
    /// </summary>
    private string formatingString;
    
    /// <summary>
    /// Inicjalizacja. Ustawia wartość maxValue oraz formatingString.
    /// </summary>
    public virtual void Awake()
    {
        textUI = GetComponent<TextMeshProUGUI>();
        maxValue = (int)System.Math.Pow(10, NumbersCount);
        formatingString = "";
        for(int i=0;i< NumbersCount; i++)
        {
            formatingString += '0';
        }
    }

    /// <summary>
    /// Rozpoczęcie, ustawienie licznika na 0.
    /// </summary>
    public virtual void Start()
    {
        SetTextValue(0);
    }

    /// <summary>
    /// Ustawienie nowej wartości licznika. Gdy wartość nie jest możliwa do wyświetlenia wyświetla nieskończoność.
    /// </summary>
    /// <param name="value">Nowa wartość licznika.</param>
    public virtual void SetTextValue(int value)
    {
        if (value != lastValue)
        {
            if (value < maxValue && value >= 0)
            {
                textUI.text = value.ToString(formatingString);
            }
            else
            {
                textUI.text = "\u221E";
            }
            lastValue = value;
        }
    }

    /// <summary>
    /// Ustawia dowolny ciąg jako wartość licznika.
    /// </summary>
    /// <param name="customString">Napis w miejsce licznika.</param>
    public virtual void SetTextValue(string customString)
    {
        textUI.text = customString;
        lastValue = 0;
    }

}
