﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Zarządca menu pauzy oraz kontroli nad czasem gry i sterowaniem podczas pauzy.
/// </summary>
public class PauseManager : MonoBehaviour
{
    /// <summary>
    /// Instancja wzorca singleton
    /// </summary>
    private static PauseManager _instance;
    /// <summary>
    /// Getter dla wzorca singleton.
    /// </summary>
    public static PauseManager Instance { get { return _instance; } }

    /// <summary>
    /// Informacja czy gra jest zapauzowana.
    /// </summary>
    public bool isPaused = false;

    /// <summary>
    /// Inicjalizacja. Sprawdzenie czy nie istnieje już inna instancja singletona.
    /// </summary>
    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        SetPause(isPaused);
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Po naciśnięciu odpowiedniego przycisku zmienia stan zapauzowania.
    /// </summary>
    public void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            TooglePause();
        }
    }

    /// <summary>
    /// Zmiana stanu zapauzowania na przeciwny.
    /// </summary>
    public void TooglePause()
    {
        isPaused = !isPaused;
        SetToState();
    }

    /// <summary>
    /// Zmiana stanu zapauzowania.
    /// </summary>
    /// <param name="pause">Nowy stan zapauzowania.</param>
    public void SetPause(bool pause)
    {
        isPaused = pause;
        SetToState();
    }

    /// <summary>
    /// Przedwczesne zakończenie gry.
    /// </summary>
    public void Surrender()
    {
        GameManager.Instance.EndGame();
    }

    /// <summary>
    /// Dopasowania parametrów gry i sterowania do stanu zapauzowania.
    /// </summary>
    private void SetToState()
    {
        AdjustTime();
        ShowHideChilds();
        ShowHideMouse();
    }

    /// <summary>
    /// Dopasowanie czasu. Gdy jest pauza, skala czasu ustawiana na 0.
    /// </summary>
    private void AdjustTime()
    {
        Time.timeScale = isPaused ? 0 : 1;
    }

    /// <summary>
    /// Pokazanie lub ukrycie menu pauzy w zależności od stanu zapauzowania.
    /// </summary>
    private void ShowHideChilds()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(isPaused);
        }
    }

    /// <summary>
    /// Udostępnienie lub ukrycie myszki w zależności od stanu zapauzowania.
    /// </summary>
    private void ShowHideMouse()
    {
        Cursor.visible = isPaused;
        Cursor.lockState = isPaused ? CursorLockMode.None : CursorLockMode.Locked;
    }

}
