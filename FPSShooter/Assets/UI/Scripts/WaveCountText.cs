﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca licznika fal na interfejsie użytkownika. Dodaje animację podczas zmiany.
/// </summary>
public class WaveCountText : CountText
{
    /// <summary>
    /// Normalna pozycja i skala licznika.
    /// </summary>
    public RectTransform normalTransform;
    /// <summary>
    /// Pozycja i skala licznika po zmianie wartości.
    /// </summary>
    public RectTransform superTransform;
    /// <summary>
    /// Czas przejscia pomiędzy skalą po zmianie wartości i normalną.
    /// </summary>
    public float transitionTime;
    /// <summary>
    /// Przełącznik określający czy zmiany mają być animowane.
    /// </summary>
    public bool animated = false;

    /// <summary>
    /// Uchwyt aktywnej korutyny animacji.
    /// </summary>
    private Coroutine activeAnimation;
    /// <summary>
    /// Referencja do komponentu aktualnej pozycji i skali.
    /// </summary>
    private RectTransform rectTransform;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        rectTransform = GetComponent<RectTransform>();
    }

    /// <summary>
    /// Ustawienie wartości licznika.
    /// </summary>
    /// <param name="value">Nowa wartość licznika.</param>
    public override void SetTextValue(int value)
    {
        base.SetTextValue(value);

        if(activeAnimation != null)
        {
            StopCoroutine(activeAnimation);
        }
        if (animated)
        {
            activeAnimation = StartCoroutine(NewValueAnimation());
        }
    }

    /// <summary>
    /// Animacja pojawienia się nowej fali.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator NewValueAnimation()
    {
        float elapsedTime = 0;

        while (elapsedTime < transitionTime)
        {
            rectTransform.localPosition = Vector3.Lerp(superTransform.localPosition,
                normalTransform.localPosition,
                elapsedTime / transitionTime);
            rectTransform.localScale = Vector3.Lerp(superTransform.localScale,
                normalTransform.localScale,
                elapsedTime / transitionTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        rectTransform.localPosition = normalTransform.localPosition;
        rectTransform.localScale = normalTransform.localScale;
    }
}
