﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Klasa reprezentująca pojedyncze gniazdo na broń w interfejsie użytkownika.
/// </summary>
public class WeaponSlotUI : MonoBehaviour
{
    /// <summary>
    /// Obraz, na którym będzie wyświetlana ikona broni.
    /// </summary>
    public Image image;
    /// <summary>
    /// Tekstowy opis klawisza do wyboru broni.
    /// </summary>
    public TextMeshProUGUI textUI;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        image = GetComponentInChildren<Image>();
        textUI = GetComponentInChildren<TextMeshProUGUI>();
    }

    /// <summary>
    /// Ustawienie ikony i opisu klawiszu broni.
    /// </summary>
    /// <param name="sprite">Nowa ikona.</param>
    /// <param name="description">Nowy opis.</param>
    public void SetWeaponSlot(Sprite sprite, string description)
    {
        image.sprite = sprite;
        textUI.text = description;
    }
}
