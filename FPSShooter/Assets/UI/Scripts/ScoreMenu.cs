﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Zarządca menu z wynikiem końcowym.
/// </summary>
public class ScoreMenu : MonoBehaviour
{
    /// <summary>
    /// Tekst na góreze menu z gratulacjami.
    /// </summary>
    public TextMeshProUGUI miscText;
    /// <summary>
    /// Tekst z ilością fal.
    /// </summary>
    public TextMeshProUGUI waveText;
    /// <summary>
    /// Tekst z najwyższym czasem gry.
    /// </summary>
    public TextMeshProUGUI timeText;

    /// <summary>
    /// Etykieta ilości fal.
    /// </summary>
    public string waveLabel;
    /// <summary>
    /// Etykieta czasu gry.
    /// </summary>
    public string timeLabel;
    /// <summary>
    /// Format wyświetlania czasu.
    /// </summary>
    public string timeFormat = "{0:00}:{1:00}:{2:00}";
    /// <summary>
    /// Tekst z gratulacjami za najwyższy wynik.
    /// </summary>
    public string highScoreCongratulations;
    /// <summary>
    /// Możliwe teksty do wyświetlenia przy braku pobicia najwyższego wyniku.
    /// </summary>
    public List<string> randomCongratulations;

    /// <summary>
    /// Numer fali.
    /// </summary>
    private int wave;
    /// <summary>
    /// Czas gry.
    /// </summary>
    private float time;

    /// <summary>
    /// Rozpoczęcie. Odczytanie z GameManager osiągniętego wyniku. Porównanie wyniku z najwyższym. Ustawienie menu.
    /// </summary>
    public void Start()
    {
        wave = GameManager.Instance.Wave;
        time = GameManager.Instance.ElapsedTime;

        SetScoreText();

        if (isNewHighScore)
        {
            miscText.text = highScoreCongratulations;
            HighScoreLoader.SaveHighScore(new HighScore(wave, time));
        }
        else
        {
            miscText.text = GetRandomCongratulations();
        }
    }

    /// <summary>
    /// Przejście do menu głównego.
    /// </summary>
    public void GoToMainMenu()
    {
        MenuScenesManager.Instance.Load(MenuScenesManager.Scene.Menu);
    }

    /// <summary>
    /// Ustawienie osiągnietego wyniku.
    /// </summary>
    private void SetScoreText()
    {
        waveText.text = waveLabel + wave;

        int hours = TimeSpan.FromSeconds(time).Hours;
        int minutes = TimeSpan.FromSeconds(time).Minutes;
        int seconds = TimeSpan.FromSeconds(time).Seconds;
        timeText.text = timeLabel + string.Format(timeFormat, hours, minutes, seconds);

    }

    /// <summary>
    /// Generator losowych gratulacji.
    /// </summary>
    /// <returns>Zwraca jeden z napisów w tablicy randomCongratulations lub "Nice!!!" przy braku tekstu w tablicy.</returns>
    private string GetRandomCongratulations()
    {
        if(randomCongratulations.Count < 1)
        {
            return "Nice!!!";
        }
        return randomCongratulations[UnityEngine.Random.Range(0, randomCongratulations.Count)];
    }

    /// <summary>
    /// Sprawdzenie czy aktualny numer fali jest wyższy od najwyższego osiągnietego do tej pory.
    /// </summary>
    private bool isNewHighScore { 
        get
        {
            HighScore previousHighScore = HighScoreLoader.LoadHighScore();
            return wave > previousHighScore.wave;
        }
    }
}
