﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zarządca wyświetlania wyboru broni w interfejsie użytkownika.
/// </summary>
public class WeaponChoose : MonoBehaviour
{
    /// <summary>
    /// Rozmiar obrazu z ikoną broni.
    /// </summary>
    public float imageSize = 80;
    /// <summary>
    /// Maksymalna ilość gniad na broń.
    /// </summary>
    public int maxSlots = 4;
    /// <summary>
    /// Prefabrykat gniazda na broń.
    /// </summary>
    public GameObject weaponSlotPrefab;

    /// <summary>
    /// Pozycja znacznika wybranej broni.
    /// </summary>
    public RectTransform mark;
    /// <summary>
    /// Czas przejscia znacznika broni na inną pozycję.
    /// </summary>
    public float markTransitionTime = 1;

    /// <summary>
    /// Lista aktualnie ustawionych gniazd broni.
    /// </summary>
    private List<WeaponSlotUI> weaponSlots;
    /// <summary>
    /// Uchwyt korutyny z aktualnym przesuwaniem znacznika wybranej broni.
    /// </summary>
    private Coroutine activeMove;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        weaponSlots = new List<WeaponSlotUI>();
    }

    /// <summary>
    /// Ustawienie ikony i opisu wybranego gniazda broni. 
    /// W przypadku, gdy indeks będzie większy aktualnej ilości, zostaną dodane nowe gniazda, aż do osiągnięcia maksymalnej ilości.
    /// </summary>
    /// <param name="index">Indeks gniazda do ustawienia.</param>
    /// <param name="sprite">Ikona broni.</param>
    /// <param name="description">Opis klawisza do wyboru broni.</param>
    public void SetWeaponSlot(int index, Sprite sprite, string description)
    {
        if(index >= maxSlots || index < 0)
        {
            return;
        }
        if(index >= weaponSlots.Count)
        {
            int indexDifference = weaponSlots.Count - index + 1;
            for (int i = 0; i < indexDifference; i++)
            {
                var newSlot = Instantiate(weaponSlotPrefab, transform);
                newSlot.transform.SetAsFirstSibling();
                weaponSlots.Add(newSlot.GetComponent<WeaponSlotUI>());
            }
            RelocateUI();
        }
        weaponSlots[index].SetWeaponSlot(sprite, description);
    }

    /// <summary>
    /// Wybranue broni o podanym indeksie. Indeks zostanie ograniczony do aktualnej liczby gniazd.
    /// </summary>
    /// <param name="index">Indeks wybranej broni.</param>
    /// <param name="withAnimation">Gdy true, przejscie znacznika będzie z animacją.</param>
    public void ChooseWeapon(int index, bool withAnimation = true)
    {
        index = Mathf.Clamp(index, 0, weaponSlots.Count - 1);

        if(activeMove != null)
        {
            StopCoroutine(activeMove);
        }
        var endPosition = weaponSlots[index].GetComponent<RectTransform>().localPosition;
        if (withAnimation)
        {
            activeMove = StartCoroutine(MoveMark(mark.localPosition, endPosition));
        }
        else
        {
            mark.localPosition = endPosition;
        }
    }

    /// <summary>
    /// Zmienieie układu gniazd na ekranie, tak by zwiększająca się ilość gniazd zawsze ustawiała cały element na środku.
    /// </summary>
    private void RelocateUI()
    {
        int slotCount = weaponSlots.Count;
        float newPositionX = -slotCount * imageSize / 2 + 40;
        for(int i = 0; i < slotCount; i++)
        {
            var rectTransform = weaponSlots[i].GetComponent<RectTransform>();
            var localPosition = rectTransform.localPosition;
            rectTransform.localPosition = new Vector3(newPositionX, localPosition.y, localPosition.z);
            newPositionX += imageSize;
        }
    }

    /// <summary>
    /// Animacja przesuwania znacznika wybranej broni.
    /// </summary>
    /// <param name="startPosition">Pozycja startowa.</param>
    /// <param name="endPosition">Poyzcja końcowa.</param>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator MoveMark(Vector3 startPosition, Vector3 endPosition)
    {
        float elapsedTime = 0;
        while (elapsedTime < markTransitionTime)
        {
            mark.localPosition = Vector3.Lerp(startPosition, endPosition, elapsedTime / markTransitionTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        mark.localPosition = endPosition;
    }
}
