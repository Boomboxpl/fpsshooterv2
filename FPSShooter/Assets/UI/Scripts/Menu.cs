﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Zarządca menu głównego.
/// </summary>
public class Menu : MonoBehaviour
{
    /// <summary>
    /// Combo-box z możliwością wyboru mapy.
    /// </summary>
    public TMP_Dropdown dropdown;
    /// <summary>
    /// Lista dostępnych poziomów.
    /// </summary>
    public List<LevelScene> possibleLevels;

    /// <summary>
    /// Indeks wybranej mapy.
    /// </summary>
    private int choosedMapIndex;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        choosedMapIndex = 0;
    }

    /// <summary>
    /// Rozpoczęcie. Ustawienie dostępnych poziomów jako pozycji w combo-box.
    /// </summary>
    public void Start()
    {
        var levelOptions = new List<TMP_Dropdown.OptionData>();
        foreach(var level in possibleLevels)
        {
            levelOptions.Add(new TMP_Dropdown.OptionData(level.DescriptiveName, level.captionSprite));
        }
        dropdown.AddOptions(levelOptions);
    }

    /// <summary>
    /// Obsługa przycisku "Play".
    /// </summary>
    public void Play()
    {
        if(choosedMapIndex == 0)
        {
            //Get random map.
            choosedMapIndex = Random.Range(0, possibleLevels.Count) + 1;
        }
        choosedMapIndex--;

        MenuScenesManager.Instance.LoadWithSplash(possibleLevels[choosedMapIndex].buildIndex);
    }

    /// <summary>
    /// Obsługa przycisku "Stats".
    /// </summary>
    public void Stats()
    {
        MenuScenesManager.Instance.Load(MenuScenesManager.Scene.StatsMenu);
    }

    /// <summary>
    /// Obsługa przycisku "Exit".
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Obsługa zmiany wybranej mapy w combo-box.
    /// </summary>
    /// <param name="newIndex">Nowy indeks wybranej mapy. Indeks z listy, nie faktyczny indeks sceny.</param>
    public void ChangeMap(int newIndex)
    {
        choosedMapIndex = newIndex;
    }

    /// <summary>
    /// Klasa reprezentująca pozycję na liście poziomów. 
    /// </summary>
    [System.Serializable]
    public class LevelScene
    {
        /// <summary>
        /// Nazwa przy wyborze.
        /// </summary>
        public string DescriptiveName;
        /// <summary>
        /// Ikona mapy.
        /// </summary>
        public Sprite captionSprite;
        /// <summary>
        /// Faktyczny indeks sceny poziomu przydzielony podczas budowy aplikacji.
        /// </summary>
        public int buildIndex;
    }

}

