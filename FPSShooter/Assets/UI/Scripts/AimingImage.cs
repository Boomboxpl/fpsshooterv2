﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zarządca celownika na interfejsie użytkownika.
/// </summary>
public class AimingImage : MonoBehaviour
{
    /// <summary>
    /// Obraz celownika.
    /// </summary>
    protected Image image;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public virtual void Awake()
    {
        image = GetComponent<Image>();
    }

    /// <summary>
    /// Ustawienie skali celownika.
    /// </summary>
    /// <param name="scale">Nowa skala.</param>
    public void SetImageScale(float scale)
    {
        image.transform.localScale = new Vector3(scale, scale, scale);
    }

    /// <summary>
    /// Ustawienie obrazu celownika.
    /// </summary>
    /// <param name="newImage">Nowy obraz celownika.</param>
    public void SetImage(Sprite newImage)
    {
        image.sprite = newImage;
    }

}
