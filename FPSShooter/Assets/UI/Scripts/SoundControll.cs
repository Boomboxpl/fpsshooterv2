﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

/// <summary>
/// Zarządca suwaków do zmiany głośności.
/// </summary>
public class SoundControll : MonoBehaviour
{
    /// <summary>
    /// Suwak do głośności muzyki.
    /// </summary>
    public Slider musicSlider;
    /// <summary>
    /// Suwak do głośności efektów dźwiękowych.
    /// </summary>
    public Slider sfxSlider;

    /// <summary>
    /// Klucz, pod którym zapisywana jest głoścnoś muzyki za pomocą PlayerPrefs.
    /// </summary>
    public string musicValuePlayerPrefs = "musicPref";
    /// <summary>
    /// Nazwa grupy muzyki w mikserze.
    /// </summary>
    public string musicGroupMixer = "music";
    /// <summary>
    /// Klucz, pod którym zapisywana jest głoścnoś efektów dźwiękowych za pomocą PlayerPrefs.
    /// </summary>
    public string sfxValuePlayerPrefs = "sfxPref";
    /// <summary>
    /// Nazwa grupy efektów dźwiękowych w mikserze.
    /// </summary>
    public string sfxGroupMixer = "sfx";
    /// <summary>
    /// Mikser dźwięku.
    /// </summary>
    public AudioMixer mixer;

    /// <summary>
    /// Po aktywowaniu obiektu pobiera wartość zapisanych głośności i ustawia wartości suwaków.
    /// </summary>
    private void OnEnable()
    {
        musicSlider.value = PlayerPrefs.GetFloat(musicValuePlayerPrefs, musicSlider.maxValue);
        sfxSlider.value = PlayerPrefs.GetFloat(sfxValuePlayerPrefs, sfxSlider.maxValue);

    }

    /// <summary>
    /// Po starcie ustawia głośność grup miksera zgodnie z danymi zapisanymi w PlayerPrefs.
    /// </summary>
    private void Start()
    {
        mixer.SetFloat(musicGroupMixer, PlayerPrefs.GetFloat(musicValuePlayerPrefs, musicSlider.maxValue));
        mixer.SetFloat(sfxGroupMixer, PlayerPrefs.GetFloat(sfxValuePlayerPrefs, sfxSlider.maxValue));
    }

    /// <summary>
    /// Zdarzenie zmiany wartości na suwaku muzyki. Zapisuje nową wartość do PlayerPrefs.
    /// </summary>
    /// <param name="newValue">Nowa wartość na suwaku.</param>
    public void OnMusicValueChange(float newValue)
    {
        PlayerPrefs.SetFloat(musicValuePlayerPrefs, newValue);
        mixer.SetFloat(musicGroupMixer, newValue);
    }

    /// <summary>
    /// Zdarzenie zmiany wartości na suwaku efektów dźwiękowych. Zapisuje nową wartość do PlayerPrefs.
    /// </summary>
    /// <param name="newValue">Nowa wartość na suwaku.</param>
    public void OnSfxValueChanged(float newValue)
    {
        PlayerPrefs.SetFloat(sfxValuePlayerPrefs, newValue);
        mixer.SetFloat(sfxGroupMixer, newValue);
    }
}
