﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca ekranu ładowania.
/// </summary>
public class LoaderCallback : MonoBehaviour
{
    /// <summary>
    /// Informacja czy jest to pierwsza aktualizacja.
    /// </summary>
    private bool isFirstUpdate = true;

    /// <summary>
    /// Ładowanie sceny za pomocą callbacku.
    /// </summary>
    private void Update()
    {
        if (!isFirstUpdate)
            return;

        isFirstUpdate = false;
        MenuScenesManager.Instance.SManagerCallback();
    }
}
