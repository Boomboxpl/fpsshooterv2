﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Zarządca interfejsu użytkownika podczas gry.
/// </summary>
public class InGameUIController : MonoBehaviour
{
    /// <summary>
    /// Instancja wzorca singleton
    /// </summary>
    private static InGameUIController _instance;
    /// <summary>
    /// Getter dla wzorca singleton.
    /// </summary>
    public static InGameUIController Instance { get { return _instance; } }

    /// <summary>
    /// Zarządca elementu interfejsu - celownik.
    /// </summary>
    public AimingImage aimUI;
    /// <summary>
    /// Zarządca elementu interfejsu - licznik kul w magazynku.
    /// </summary>
    public CountText magazineText;
    /// <summary>
    /// Zarządca elementu interfejsu - licznik kul w zapasie.
    /// </summary>
    public CountText bulletsText;
    /// <summary>
    /// Zarządca elementu interfejsu - licznik wrogów.
    /// </summary>
    public CountText enemyCountText;
    /// <summary>
    /// Zarządca elementu interfejsu - licznik fal.
    /// </summary>
    public WaveCountText waveText;
    /// <summary>
    /// Zarządca elementu interfejsu - pasek życia.
    /// </summary>
    public HealthBar healthBar;
    /// <summary>
    /// Zarządca elementu interfejsu - wybór broni.
    /// </summary>
    public WeaponChoose weaponChoose;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if(!isInitialized)
        {
            Debug.LogWarning("No properly setted ui components");
        }
    }

    /// <summary>
    /// UStawienie ilości kul w magazynku i w zapasie.
    /// </summary>
    /// <param name="magazine">Ilość kul w magazynku.</param>
    /// <param name="bullets">Ilość kul w zapasie.</param>
    public void SetMagazineAndBullets(int magazine, int bullets)
    {
        magazineText.SetTextValue(magazine);
        bulletsText.SetTextValue(bullets);
    }

    /// <summary>
    /// Ustawienie numeru fali.
    /// </summary>
    /// <param name="wave">Nowy numer fali.</param>
    public void SetWave(int wave)
    {
        waveText.animated = true;
        waveText.SetTextValue(wave);
    }

    /// <summary>
    /// Właściwość określająca czy interfejs użytkownika jest poprawnie ustawiony.
    /// </summary>
    public bool isInitialized => aimUI != null && magazineText != null && bulletsText != null && healthBar != null && weaponChoose != null;
}
