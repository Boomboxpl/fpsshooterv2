﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Zarządca menu statystyk.
/// </summary>
public class StatsMenu : MonoBehaviour
{
    /// <summary>
    /// Tekst z ilością fal.
    /// </summary>
    public TextMeshProUGUI waveText;
    /// <summary>
    /// Tekst z czasem gry.
    /// </summary>
    public TextMeshProUGUI timeText;

    /// <summary>
    /// Etykieta ilości fal.
    /// </summary>
    public string waveLabel;
    /// <summary>
    /// Etykieta czasu gry.
    /// </summary>
    public string timeLabel;
    /// <summary>
    /// Format wyświetlania czasu.
    /// </summary>
    public string timeFormat = "{0:00}:{1:00}:{2:00}";

    /// <summary>
    /// Rozpoczęcie. Ustawia wynik na odczytany z pliku.
    /// </summary>
    private void Start()
    {
        SetScore();
    }

    /// <summary>
    /// Przejście do menu głównego.
    /// </summary>
    public void GoToMainMenu()
    {
        MenuScenesManager.Instance.Load(MenuScenesManager.Scene.Menu);
    }

    /// <summary>
    /// Reset najwyższego wyniku.
    /// </summary>
    public void ResetScore()
    {
        HighScoreLoader.ResetHighScore();
        SetScore();
    }

    /// <summary>
    /// Ustawienie wyniku na odczytany z pliku.
    /// </summary>
    private void SetScore()
    {
        var score = HighScoreLoader.LoadHighScore();
        waveText.text = waveLabel + score.wave;

        float time = score.time;
        int hours = TimeSpan.FromSeconds(time).Hours;
        int minutes = TimeSpan.FromSeconds(time).Minutes;
        int seconds = TimeSpan.FromSeconds(time).Seconds;
        timeText.text = timeLabel + string.Format(timeFormat, hours, minutes, seconds);
    }
}
