﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Zarządca ładowania scen menu oraz gry.
/// </summary>
public class MenuScenesManager
{
    /// <summary>
    /// Intancja wzorca singleton.
    /// </summary>
    private static MenuScenesManager instance;

    /// <summary>
    /// Getter dla wzorca singleton.
    /// </summary>
    public static MenuScenesManager Instance
    {
        get
        {
            if (instance == null)
                instance = new MenuScenesManager();

            return instance;
        }
    }

    /// <summary>
    /// Rodzaje scen menu.
    /// </summary>
    public enum Scene 
    { 
        /// <summary>
        /// Scena menu głównego.
        /// </summary>
        Menu = 0, 
        /// <summary>
        /// Scena ekranu ładowania.
        /// </summary>
        LoadingScreen = 1, 
        /// <summary>
        /// Scena menu z wynikiem.
        /// </summary>
        ScoreMenu = 2, 
        /// <summary>
        /// Scena menu z najwyższymi wynikami.
        /// </summary>
        StatsMenu = 3
    }

    /// <summary>
    /// Callback wywoływany po załadowaniu sceny z ekranem ładowania.
    /// </summary>
    private Action onSManagerCallback;

    /// <summary>
    /// Załadowanie sceny z ekranem ładowania.
    /// </summary>
    /// <param name="scene">Scena do załadowania.</param>
    public void LoadWithSplash(Scene scene)
    {
        LoadWithSplash((int)scene);
    }

    /// <summary>
    /// Załadowanie sceny z ekranem ładowania.
    /// </summary>
    /// <param name="sceneBuildIndex">Indeks sceny.</param>
    public void LoadWithSplash(int sceneBuildIndex)
    {
        // Set the SManager callback action to load the target scene
        onSManagerCallback = () =>
        {
            SceneManager.LoadScene(sceneBuildIndex);
        };

        SceneManager.LoadScene((int)Scene.LoadingScreen);
    }

    /// <summary>
    /// Załadowanie sceny (bez ekranu ładowania).
    /// </summary>
    /// <param name="scene">Scena do załadowania.</param>
    public void Load(Scene scene)
    {
        SceneManager.LoadScene((int)scene);
    }

    /// <summary>
    /// Wywołanie callbacka po załadowaniu sceny.
    /// </summary>
    public void SManagerCallback()
    {
        if(onSManagerCallback != null)
        {
            onSManagerCallback();
            onSManagerCallback = null;
        }
    }
}
