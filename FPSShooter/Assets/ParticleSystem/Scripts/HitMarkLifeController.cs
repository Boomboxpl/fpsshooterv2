﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zarządca czasu życia systemów cząsteczkowych.
/// </summary>
[RequireComponent(typeof(ParticleSystem))]
public class HitMarkLifeController : MonoBehaviour
{
    /// <summary>
    /// Inicjalizacja. Rozpoczęcie pracy systemu cząsteczkowego i ustawienie czasu zniszczenia na długość trwania efektu.
    /// </summary>
    void Awake()
    {
        var part = GetComponent<ParticleSystem>();
        part.Play();
        Destroy(gameObject, part.main.duration);
    }
}
