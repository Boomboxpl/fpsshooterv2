﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Kontroler starowania postacią gracza przez użytkownika.
/// </summary>
public class InputController : MonoBehaviour
{
    /// <summary>
    /// Szybkość marszu.
    /// </summary>
    [Header("Moving Parameters")]
    public float walkingSpeed = 7.5f;
    /// <summary>
    /// Szybkość biegu.
    /// </summary>
    public float runningSpeed = 11.5f;
    /// <summary>
    /// Szybkość skoku.
    /// </summary>
    public float jumpSpeed = 8.0f;
    /// <summary>
    /// Przyciąganie gracza.
    /// </summary>
    public float gravity = 20.0f;
    /// <summary>
    /// Szybkość rozglądania się.
    /// </summary>
    public float lookSpeed = 2.0f;
    /// <summary>
    /// Maksymalny kąt patrzenia w górę/dół.
    /// </summary>
    [Range(0, 90)]
    public float lookXLimit = 45.0f;
    /// <summary>
    /// Próg, od którego zostaje uznane przekręcenie rolki myszy.
    /// </summary>
    public float mouseRollTreshold = 0.1f;

    /// <summary>
    /// Współczynnik odrzutu podczas biegu.
    /// </summary>
    [Header("Recoil from Moving")]
    public float runningRecoil = 1;
    /// <summary>
    /// Współczynnik odrzutu podczas skoku.
    /// </summary>
    public float jumpingRecoil = 1;
    /// <summary>
    /// Współczynnik odrzutu podczas marszu.
    /// </summary>
    public float walkingRecoil = 0.4f;

    /// <summary>
    /// Pole informujące czy postać może się poruszać.
    /// </summary>
    [HideInInspector]
    public bool canMove = true;
    /// <summary>
    /// Komponent odpowiedzialny za ruch postaci.
    /// </summary>
    private CharacterController characterController;

    /// <summary>
    /// Kierunek poruszania się.
    /// </summary>
    private Vector3 moveDirection = Vector3.zero;
    /// <summary>
    /// Rotatcja względem osi X.
    /// </summary>
    private float rotationX = 0;
    /// <summary>
    /// Rotacja względem osi Y.
    /// </summary>
    private float rotationY = 0;
    /// <summary>
    /// Pole informujące czy się biegnie.
    /// </summary>
    private bool isRunning;
    /// <summary>
    /// Pole informujące czy w ostatniej klatce było się na ziemi.
    /// </summary>
    private bool wasGrounded = true;

    /// <summary>
    /// Komponent gracza.
    /// </summary>
    private Player player;
    /// <summary>
    /// Komponent odpowiedzialny za dźwięk.
    /// </summary>
    private CharacterAudioController characterAudio;

    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    [Header("Input Mapping")]
    public string verticalAxis = "Vertical";
    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    public string horizontalAxis = "Horizontal";
    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    public string mouseAxisX = "Mouse X";
    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    public string mouseAxisY = "Mouse Y";
    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    public string jumpButton = "Jump";
    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    public string mouseScroll = "Mouse ScrollWheel";
    /// <summary>
    /// Mapowanie InputManagera.
    /// </summary>
    public string runButton = "Run";
    /// <summary>
    /// Lista akcji dostępnych po naciśnięciu przycisku.
    /// </summary>
    public List<InputEvent> buttonActions;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        player = GetComponent<Player>();
        characterAudio = GetComponent<CharacterAudioController>();
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Akcje, ruch i dźwięk są aktualizowane, jeżeli nie ma pauzy.
    /// </summary>
    private void Update()
    {
        if (!PauseManager.Instance.isPaused)
        {
            UpdateMovement();
            UpdateWeapons();
            characterAudio.UpdateSound();
        }
    }

    /// <summary>
    /// Otrzymanie wartości odrzutu na podstawie aktualnego stanu ruchu.
    /// </summary>
    /// <returns>Czynnik odrzutu.</returns>
    public float GetRecoilFromMoveValue()
    {
        if (IsWalking)
        {
            if (isRunning)
            {
                return runningRecoil;
            }
            else
            {
                return walkingRecoil;
            }
        }
        else if (!characterController.isGrounded)
        {
            return jumpingRecoil;
        }
        return 0;
    }

    /// <summary>
    /// Aktualizacja akcji i zmiany broni.
    /// </summary>
    private void UpdateWeapons()
    {
        foreach (var buttonAction in buttonActions)
        {
            buttonAction.Invoke();
        }

        if (Mathf.Abs(Input.GetAxis(mouseScroll)) > mouseRollTreshold)
        {
            player.ChangeWeaponToNext(Input.mouseScrollDelta.y > 0 ? 1 : -1);
        }

        player.SetRecoilFromMove(GetRecoilFromMoveValue());
    }

    /// <summary>
    /// Aktualizacja ruchu.
    /// </summary>
    private void UpdateMovement()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);

        isRunning = Input.GetButton(runButton);
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis(verticalAxis) : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis(horizontalAxis) : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        if (IsWalking)
        {
            characterAudio.state = characterAudio.state = CharacterState.Walking;
            if (isRunning)
            {
                characterAudio.state = characterAudio.state = CharacterState.Running;
            }
        }
        else
        {
            characterAudio.state = CharacterState.Idle;
        }

        if (IsJumping)
        {
            characterAudio.state = characterAudio.state = CharacterState.Jumping;
            moveDirection.y = jumpSpeed;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }

        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        if (IsLanding)
        {
            characterAudio.state = characterAudio.state = CharacterState.Landing;
        }
        wasGrounded = characterController.isGrounded;

        // Move object
        characterController.Move(moveDirection * Time.deltaTime);

        // Player and childs rotation
        if (canMove)
        {
            rotationX -= Input.GetAxis(mouseAxisY) * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
            rotationY += Input.GetAxis(mouseAxisX) * lookSpeed;
            transform.rotation = Quaternion.identity;
            transform.Rotate(Vector3.right, rotationX, Space.World);
            transform.Rotate(Vector3.up, rotationY, Space.World);
        }
    }

    /// <summary>
    /// Własność informująca czy postać idzie.
    /// </summary>
    private bool IsWalking { get { return new Vector2(moveDirection.x, moveDirection.z) != Vector2.zero && characterController.isGrounded; } }
    /// <summary>
    /// Własność informująca czy postać biegnie.
    /// </summary>
    private bool IsJumping { get { return Input.GetButton(jumpButton) && canMove && characterController.isGrounded; } }
    /// <summary>
    /// Własność informująca czy postać wylądowała.
    /// </summary>
    private bool IsLanding { get { return !wasGrounded && characterController.isGrounded; } }
}
