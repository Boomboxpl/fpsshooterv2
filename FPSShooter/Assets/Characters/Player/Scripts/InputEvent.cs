﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Klasa definująca wywoływane funkcje na podstawie klasy Input.
/// </summary>
[System.Serializable]
public class InputEvent
{
    /// <summary>
    /// Nazwa klawisza w InputManagerze.
    /// </summary>
    public string inputKey;
    /// <summary>
    /// Rodzaj wywoływania akcji.
    /// </summary>
    public TypeOfInput type;
    /// <summary>
    /// Wywoływana funkcja.
    /// </summary>
    [SerializeField]
    public UnityEvent Function;

    /// <summary>
    /// Sprawdzenie czy jest działanie jest aktywne i wywołanie odpowieniej funkcji.
    /// </summary>
    public void Invoke()
    {
        if (DownActive || RepatableHighActive || UpActive || RepatableLowActive)
        {
            Function.Invoke();
        }
    }

    /// <summary>
    /// Czy funkcja aktywna kliknięciem powinna zostać wywołana.
    /// </summary>
    public bool DownActive => Input.GetButtonDown(inputKey) && type == TypeOfInput.Down;

    /// <summary>
    /// Czy funkcja aktywna przytrzymaniem powinna zostać wywołana.
    /// </summary>
    public bool RepatableHighActive => Input.GetButton(inputKey) && type == TypeOfInput.RepeatableHigh;

    /// <summary>
    /// Czy funkcja aktywna puszczeniem powinna zostać wywołana.
    /// </summary>
    public bool UpActive => Input.GetButtonUp(inputKey) && type == TypeOfInput.Up;

    /// <summary>
    /// Czy funkcja aktywna brakiem przytrzymania powinna zostać wywołana.
    /// </summary>
    public bool RepatableLowActive => !Input.GetButton(inputKey) && type == TypeOfInput.RepeatableLow;

    /// <summary>
    /// Rodzaje aktywności.
    /// </summary>
    public enum TypeOfInput 
    { 
        /// <summary>
        /// Przyciśnięcie.
        /// </summary>
        Down, 
        /// <summary>
        /// Przytrzymanie.
        /// </summary>
        RepeatableHigh, 
        /// <summary>
        /// Puszczenie.
        /// </summary>
        Up, 
        /// <summary>
        /// Brak przytrzymania.
        /// </summary>
        RepeatableLow
    };
}
