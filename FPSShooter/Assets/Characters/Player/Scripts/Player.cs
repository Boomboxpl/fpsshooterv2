﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa gracza.
/// </summary>
public class Player : MonoBehaviour, DestroyAble
{
    /// <summary>
    /// Właściwość określająca poziom życia.
    /// </summary>
    public float Life { get; private set;}
    /// <summary>
    /// Właściwość określająca czy przeciwnik jest żywy.
    /// </summary>
    public bool IsAlive { get => Life > 0; }

    /// <summary>
    /// Odległość, w której można podnieść skrzynkę.
    /// </summary>
    [SerializeField]
    private float grabDistance = 1;
    /// <summary>
    /// Maksymalna ilość noszonej broni.
    /// </summary>
    [SerializeField]
    private int maxWeaponCount = 4;

    /// <summary>
    /// Maksymalny poziom życia.
    /// </summary>
    [SerializeField]
    private float maxLife = 100;

    /// <summary>
    /// Prefabrykaty broni. Znajdujące się na liście podczas startu zostaną utworzone jako startowe bronie.
    /// </summary>
    [Header("Starting Weapon Configuration")]
    [SerializeField]
    private List<GameObject> weaponsPrefabs;
    /// <summary>
    /// Referencje do aktualnych broni.
    /// </summary>
    private List<Weapon> weapons;
    /// <summary>
    /// Referencje do obiektów broni.
    /// </summary>
    private List<GameObject> weaponsObjects;
    /// <summary>
    /// Indeks pod którą znajduje się broń biała.
    /// </summary>
    [SerializeField]
    private int meleeWeaponIndex = 0;
    /// <summary>
    /// Indeks podstawowej broni - wybieranej na początku gry.
    /// </summary>
    [SerializeField]
    private int primaryWeapon = 1;
    /// <summary>
    /// Indeks wybranej broni.
    /// </summary>
    private int chosedWeapon = 0;

    /// <summary>
    /// Czas umierania.
    /// </summary>
    [Header("Dying Parameters")]
    [SerializeField]
    private float dyingTime = 1;

    /// <summary>
    /// Komponent przetwarzania końcowego ekranu względem otrzymanych obrażeń.
    /// </summary>
    private TintCameraPP tintCamera;

    /// <summary>
    /// Referencja do komponentu odpowiedzialnego za dźwięk.
    /// </summary>
    private CharacterAudioController characterAudioController;

    /// <summary>
    /// Pozycja w której będą chowane i wyciągane bronie.
    /// </summary>
    [Header("Weapon Change Parameters")]
    public Transform weaponSack;
    /// <summary>
    /// Pole inforumujące czy broń jest aktualnie zmieniania.
    /// </summary>
    private bool isChanging;
    /// <summary>
    /// Indeks nowo wybranej broni.
    /// </summary>
    private int newWeapon;
    /// <summary>
    /// Czas chowania/wyciągania broni.
    /// </summary>
    [SerializeField]
    private float hideShowTime = 0.1f;

    /// <summary>
    /// Funkcja edytora, pozwalająca na łatwe ustawienie zasięgu podnoszenia.
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, grabDistance);
    }

    /// <summary>
    /// Inicjalizacja. Ustawiane są odpowiednie bronie na podstawie aktywnych prefabów broni. Ustalany jest poziom życia na maksymalny.
    /// </summary>
    virtual public void Awake()
    {
        weapons = new List<Weapon>();
        weaponsObjects = new List<GameObject>();
        Life = maxLife;
        tintCamera = GetComponentInChildren<TintCameraPP>();
        foreach(var prefab in weaponsPrefabs)
        {
            InstantiateWeapon(prefab);
        }
        characterAudioController = GetComponent<CharacterAudioController>();
    }

    /// <summary>
    /// Ustawienie odpowiednych broni jako wybranej, ukrycie innych. 
    /// Ustawienie startowe interfejsu użytkownika. Rozpoczęcie gry po jednej klatce
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    virtual public IEnumerator Start()
    {
        for(int i = 0; i< weaponsObjects.Count; i++)
        {
            PrepareWeapon(i);
        }
        chosedWeapon = primaryWeapon;
        weaponsObjects[chosedWeapon].SetActive(true);

        //Wait for everything set up and start game.
        yield return null;
        InGameUIController.Instance.weaponChoose.ChooseWeapon(chosedWeapon, false);
        RefreshBulletsUI();
        InGameUIController.Instance.healthBar.SetHealth(Life, maxLife);
        GameManager.Instance.StartGame();
    }

    /// <summary>
    /// Funckja wywołująca strzał z broni.
    /// </summary>
    virtual public void Shoot()
    {
        if(weapons[chosedWeapon] != null && !isChanging)
        {
            weapons[chosedWeapon].Shoot(transform.position, transform.forward);
            RefreshBulletsUI();
        }
    }

    /// <summary>
    /// Funkcja zwalniająca spust broni.
    /// </summary>
    virtual public void FreeTrigger()
    {
        if (weapons[chosedWeapon] != null && !isChanging)
        {
            weapons[chosedWeapon].FreeTrigger();
        }
    }

    /// <summary>
    /// Przeładowanie broni.
    /// </summary>
    virtual public void Reload()
    {
        if (weapons[chosedWeapon] != null && !isChanging)
        {
            weapons[chosedWeapon].Reload();
        }
    }

    /// <summary>
    /// Wykonanie szybkiego ataku.
    /// </summary>
    virtual public void QuickAttack()
    {
        if (!isChanging)
        {
            StartCoroutine(QuickAttackRoutine());
        }
    }

    /// <summary>
    /// Próba podniesienia skrzynki.
    /// </summary>
    virtual public void TryGrab()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, grabDistance))
        {
            var crate = hit.collider.gameObject.GetComponent<SupplyCrate>();
            if(crate != null)
            {
                crate.Resupply(this);
            }
        }
    }

    /// <summary>
    /// Dodanie broni. Jeżeli już jest taka aktywna, następuje odnowienie amunicji.
    /// </summary>
    /// <param name="prefab">Prefab nowej broni</param>
    /// <returns>True, gdy udało się podnieść broń lub załadować amunicję.</returns>
    virtual public bool AddWeapon(GameObject prefab)
    {
        bool wasResupplied = false;
        if(ResuplyAmunition(prefab, out wasResupplied)){
            return wasResupplied;
        }

        if ((weaponsPrefabs.Count + 1) > maxWeaponCount)
        {
            ReplaceWeapon(prefab);

        }
        else
        {
            GrabNewWeapon(prefab);
        }
        return true;
    }

    /// <summary>
    /// Leczenie gracza.
    /// </summary>
    /// <param name="healValue">Wartość przywróconego życia.</param>
    /// <returns>True, gdy udało się przywrócić życie.</returns>
    virtual public bool Heal(float healValue)
    {
        if(maxLife - Life < float.Epsilon)
        {
            return false;
        }

        Life += healValue;
        Life = Mathf.Clamp(Life, 0, maxLife);
        InGameUIController.Instance.healthBar.SetHealth(Life, maxLife);
        return true;
    }

    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    /// <param name="damage">Ilość otrzymanych obrażeń.</param>
    /// <param name="hitPoint">Miejsce otrzymania obrażeń.</param>
    virtual public void TakeDamage(float damage, Vector3 hitPoint)
    {
        Life -= damage;
        characterAudioController.state = CharacterState.Suffering;
        tintCamera.tintValue = 0.7f;
        InGameUIController.Instance.healthBar.SetHealth(Life, maxLife);
        if (!IsAlive)
        {
            characterAudioController.state = CharacterState.Dying;
            StartCoroutine(Dying());
        }
        characterAudioController.UpdateSound();
    }

    /// <summary>
    /// Zmiana broni na podstawie podanego indeksu.
    /// </summary>
    /// <param name="newChosed">Idneks nowo wybranej broni.</param>
    virtual public void ChangeWeapon(int newChosed)
    {
        if (newChosed >= weapons.Count)
        {
            newChosed = 0;
        }
        else if (newChosed < 0)
        {
            newChosed = weapons.Count - 1;
        }

        if (!isChanging && chosedWeapon != newChosed)
        {
            newWeapon = newChosed;
            StartCoroutine(HideShowWeapon(weaponsObjects[newChosed], weaponsObjects[chosedWeapon], weaponsPrefabs[newChosed].transform));
            InGameUIController.Instance.weaponChoose.ChooseWeapon(newChosed);
        }
    }

    /// <summary>
    /// Zmiana broni na podstawie zmiany wartości indeksu.
    /// </summary>
    /// <param name="direction">Liczba dodawana do aktualnego indeksu wybranej broni.</param>
    virtual public void ChangeWeaponToNext(int direction)
    {
        var newChose = chosedWeapon + direction;
        ChangeWeapon(newChose);
    }

    /// <summary>
    /// Ustawienie odrzyutu broni wynikającego z ruchu.
    /// </summary>
    /// <param name="value">Wartość odrzutu.</param>
    virtual public void SetRecoilFromMove(float value)
    {
        var weapon = weapons[chosedWeapon] as RecoilingWeapon;
        if(weapon != null)
        {
            weapon.RecoilMagnitude = Mathf.Max(weapon.RecoilMagnitude, value);
        }
    }

    /// <summary>
    /// Odnowienie amunicji.
    /// </summary>
    /// <param name="prefab">Prefabrykat broni.</param>
    /// <param name="resupplyResult">True, gdy udało się odnowić zapas amunicji.</param>
    /// <returns>True, gdy znaleziono broń identyczną do prefabrykatu</returns>
    private bool ResuplyAmunition(GameObject prefab, out bool resupplyResult)
    {
        for (int i = 0; i < weaponsPrefabs.Count; i++)
        {
            if (prefab == weaponsPrefabs[i])
            {
                resupplyResult = weapons[i].ResupplyBullets();
                RefreshBulletsUI();
                return true;
            }
        }
        resupplyResult = false;
        return false;
    }

    /// <summary>
    /// Procedura podniesienia nowej broni.
    /// </summary>
    /// <param name="prefab">Prefabrykat broni.</param>
    private void GrabNewWeapon(GameObject prefab)
    {
        weaponsPrefabs.Add(prefab);
        InstantiateWeapon(prefab);
        PrepareWeapon(weapons.Count - 1);
        ChangeWeapon(weapons.Count - 1);
    }

    /// <summary>
    /// Procedura zmiany broni na inną.
    /// </summary>
    /// <param name="prefab">Prefabrykat broni.</param>
    private void ReplaceWeapon(GameObject prefab)
    {
        var indexToErase = chosedWeapon;
        if (indexToErase == meleeWeaponIndex)
        {
            indexToErase++;
            if (indexToErase >= weaponsPrefabs.Count)
            {
                indexToErase = 0;
            }
        }
        weaponsPrefabs[indexToErase] = prefab;
        Destroy(weaponsObjects[indexToErase]);
        weaponsObjects[indexToErase] = Instantiate(prefab, transform);
        weapons[indexToErase] = weaponsObjects[indexToErase].GetComponent<Weapon>();
        
        PrepareWeapon(indexToErase);
        ChangeWeapon(indexToErase - 1);
    }

    /// <summary>
    /// Instancjonowanie broni z prefabrykatu.
    /// </summary>
    /// <param name="prefab">Prefabrykat broni.</param>
    /// <returns>True, gdy udało sie zinstancjonować broń.</returns>
    private bool InstantiateWeapon(GameObject prefab)
    {
        if (prefab == null)
        {
            return false;
        }
        var weapon = prefab.GetComponent<Weapon>();
        if (weapon == null)
        {
            return false;
        }
        var newWeapon = Instantiate(prefab, transform);
        weaponsObjects.Add(newWeapon);
        weapons.Add(newWeapon.GetComponent<Weapon>());

        return true;
    }

    /// <summary>
    /// Przygotowanie broni. Ustawinie jako aktywną i przygotowanie miejsca w interfejsie użytkownika.
    /// </summary>
    /// <param name="index">Indeks broni do przygotowania.</param>
    private void PrepareWeapon(int index)
    {
        InGameUIController.Instance.weaponChoose.SetWeaponSlot(index, weapons[index].Icon, (index + 1).ToString());
        weaponsObjects[index].SetActive(false);
    }

    /// <summary>
    /// Odświeżenie ilości amunicji w interfejsie użytkownika na podstawie wybraniej broni.
    /// </summary>
    private void RefreshBulletsUI()
    {
        InGameUIController.Instance.SetMagazineAndBullets(weapons[chosedWeapon].Magazine, weapons[chosedWeapon].BulletsLeft);
    }

    /// <summary>
    /// Wywoływana w momencie schowania broni i przed pojawieniem się nowej.
    /// </summary>
    /// <param name="newShowed">Obiekt nowej aktywnej broni.</param>
    /// <param name="newHidden">Obiekt ukrywanej broni.</param>
    private void ChangeCallback(GameObject newShowed, GameObject newHidden)
    {
        weapons[chosedWeapon].OnHide();
        newHidden.SetActive(false);
        chosedWeapon = newWeapon;
        newShowed.SetActive(true);
        weapons[chosedWeapon].OnShow();
        RefreshBulletsUI();
    }

    /// <summary>
    /// Korutna wykonania szybkiego ataku.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator QuickAttackRoutine()
    {
        var previousChoosed = chosedWeapon;
        ChangeWeapon(meleeWeaponIndex);

        while (isChanging)
        {
            yield return null;
        }

        Shoot();

        while (!weapons[chosedWeapon].IsReady)
        {
            yield return null;
        }

        ChangeWeapon(previousChoosed);
    }

    #region Animations
    /// <summary>
    /// Korutyna zmiany broni.
    /// </summary>
    /// <param name="newShowed">Obiekt nowej aktywnej broni.</param>
    /// <param name="newHidden">Obiekt ukrywanej broni.</param>
    /// <param name="showedPosition">Pozycja na któej ma się znaleźć nowa broń.</param>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator HideShowWeapon(GameObject newShowed, GameObject newHidden, Transform showedPosition)
    {

        isChanging = true;
        yield return AnimateHideShow(newHidden, newHidden.transform, weaponSack);

        ChangeCallback(newShowed, newHidden);

        yield return AnimateHideShow(newShowed, weaponSack, showedPosition);
        isChanging = false;
    }

    /// <summary>
    /// Animacja chowania/wyciągania broni.
    /// </summary>
    /// <param name="weapon">Obiekt broni.</param>
    /// <param name="start">Pozycja startowa.</param>
    /// <param name="end">Pozycja końcowa.</param>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator AnimateHideShow(GameObject weapon, Transform start, Transform end)
    {
        float elapsedTime = 0;
        var startPosition = start.localPosition;
        var startRotation = start.localRotation;

        var endPosition = end.localPosition;
        var endRotation = end.localRotation;

        while (elapsedTime <= hideShowTime)
        {
            weapon.transform.localPosition = Vector3.Lerp(startPosition, endPosition, elapsedTime / hideShowTime);
            weapon.transform.localRotation = Quaternion.Slerp(startRotation, endRotation, elapsedTime / hideShowTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        weapon.transform.localPosition = endPosition;
        weapon.transform.localRotation = endRotation;
    }

    /// <summary>
    /// Korutyna umierania.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator Dying()
    {
        GetComponent<CharacterController>().enabled = false;
        GetComponent<InputController>().enabled = false;
        var timeRemain = dyingTime;
        var startRotation = transform.rotation;
        var startEuler = startRotation.eulerAngles;
        startEuler.x = -90;
        var endRotation = Quaternion.Euler(startEuler);
        while ((timeRemain -= Time.deltaTime) > 0)
        {
            transform.rotation = Quaternion.Slerp(endRotation, startRotation, timeRemain / dyingTime);
            yield return null;
        }
        GameManager.Instance.EndGame();

    }
    #endregion
}
