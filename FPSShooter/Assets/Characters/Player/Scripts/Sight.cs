﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Komponent przybliżający kamerę.
/// </summary>
public class Sight : MonoBehaviour
{
    /// <summary>
    /// FOV dla przybliżenia.
    /// </summary>
    public float zoomedFov = 20;
    /// <summary>
    /// Początkowy FOV.
    /// </summary>
    public float normalFov = 0;
    /// <summary>
    /// Szybkość zmiany FOV.
    /// </summary>
    public float zoomSpeed = 10;

    /// <summary>
    /// Komponent kamery.
    /// </summary>
    private Camera camera;
    /// <summary>
    /// Aktualna skala przybliżenia.
    /// </summary>
    private float zoomScale;
    /// <summary>
    /// Pole informujące czy jest aktywne przybliżenie.
    /// </summary>
    private bool isZoomed;

    /// <summary>
    /// Inicjalizacja komponentu kamery.
    /// </summary>
    private void Awake()
    {
        camera = GetComponent<Camera>();
        if(normalFov == 0)
        {
            normalFov = camera.fieldOfView;
        }
    }

    /// <summary>
    /// Aktualizacja w każdej klatce, tak by aktualna skala zmieniała się w sposób ciągły do aktualnego stanu. 
    /// </summary>
    private void Update()
    {
        zoomScale += (isZoomed ? zoomSpeed : -zoomSpeed) * Time.deltaTime;
        zoomScale = Mathf.Clamp01(zoomScale);
        camera.fieldOfView = Mathf.Lerp(normalFov, zoomedFov, zoomScale);

    }

    /// <summary>
    /// Ustawienie przybliżenia.
    /// </summary>
    public void Zoom()
    {
        isZoomed = true;
    }

    /// <summary>
    /// Powrót z przybliżenia.
    /// </summary>
    public void Return()
    {
        isZoomed = false;
    }

   
}
