﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa abstrakcyjna roszerzająca StandardEnemy o podstawowe właściwości potrzebne do zaimplementowania przeciwnika miotającego.
/// </summary>
public abstract class ThrowingEnemy : StandardEnemy
{
    /// <summary>
    /// Prefabrykaty pocisków.
    /// </summary>
    public List<GameObject> projectilePrefabs;
    /// <summary>
    /// Czas pomiędzy atakami.
    /// </summary>
    public float TimeBetweenAttacks = 1;

    /// <summary>
    /// Pozycja, z której będą wyrzucane pociski.
    /// </summary>
    [SerializeField]
    private Transform shootingPosition;

    /// <summary>
    /// Ilość czasu pomiędzy rozpoczęciem ataku od wyrzucenia pocisku.
    /// </summary>
    [SerializeField]
    private float throwOffsetTime = 0;
    /// <summary>
    /// Ilość czasu do wyjścia z pozycji ataku po wyrzuceniu pocisku.
    /// </summary>
    [SerializeField]
    private float attackEndTimeAfterThrow = 0;

    /// <summary>
    /// Czas, który upłynął od ostatniego ataku.
    /// </summary>
    private float elapsedTimeFromLastAttack = 0;

    /// <summary>
    /// Inicjalizacja. Rozpoczęcie pogoni za graczem.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        StartHuntPlayer(0.5f, 10f);
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Aktualizuje czas pomiędzy atakami.
    /// </summary>
    public override void Update()
    {
        base.Update();
        elapsedTimeFromLastAttack += Time.deltaTime;

    }

    /// <summary>
    /// Porcedura ataku, jeśli możliwy.
    /// </summary>
    public override void Attack()
    {
        if (CanAttack)
        {
            StartCoroutine(AttackRoutine());
        }
    }

    /// <summary>
    /// Sprawdzenie czy pomiędzy graczem i przeciwnikiem znajdują się przeszkody. Zwraca True, gdy nie ma przeszkód.
    /// </summary>
    virtual public bool NoObstacles {
        get {
            LayerMask mask = LayerMask.GetMask("Character");
            int maskValue = mask.value;
            maskValue = ~maskValue;
            return !Physics.Linecast(transform.position, player.transform.position, maskValue);
        }
    }

    /// <summary>
    /// Wyliczenie kierunku rzutu pocisku.
    /// </summary>
    /// <returns>Kierunek rzutu.</returns>
    virtual protected Vector3 GetProjectileDirection()
    {
        return (player.transform.position - shootingPosition.position).normalized;
    }

    /// <summary>
    /// Korutyna ataku. Tworzy po odczekaniu pocisk poruszający się w stronę gracza.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    protected override IEnumerator AttackRoutine()
    {
        animator.SetTrigger(IsAttackingID);
        isAttacking = true;
        yield return new WaitForSeconds(throwOffsetTime);

        if (IsAlive)
        {
            elapsedTimeFromLastAttack = 0;
            var projectile = Projectile.ThrowProjectile(GetRandomProjectile(), shootingPosition.position, GetProjectileDirection());
            EndOfAttackCallback();
        }

        yield return new WaitForSeconds(attackEndTimeAfterThrow);
        isAttacking = false;
    }

    /// <summary>
    /// Sprawdzenie czy można wykonać atak (nie ma ataku, nie ma przeszkód, odczekano odpowiedni czas).
    /// </summary>
    virtual protected bool CanAttack => !isAttacking && NoObstacles && elapsedTimeFromLastAttack > TimeBetweenAttacks && IsPlayerInDistance(Vector3.up);

    /// <summary>
    /// Referencja do losowego pocisku z dostępnych.
    /// </summary>
    /// <returns>Referencja do losowego prefabu znajdującego się w projectilePrefabs.</returns>
    private GameObject GetRandomProjectile()
    {
        return projectilePrefabs[Random.Range(0, projectilePrefabs.Count)];
    }
}
