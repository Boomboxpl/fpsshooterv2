﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pocisk, który dodatkowo obraca się wokół własnej osi z losową prędkością.
/// </summary>
public class RotatingProjectile : Projectile
{
    /// <summary>
    /// Dodanie do bazowego ciała szytwnego ruchu kątowego.
    /// </summary>
    /// <param name="targetDirection">Kierunek poruszania się.</param>
    public override void SetRigidbody(Vector3 targetDirection)
    {
        base.SetRigidbody(targetDirection);
        var angularRotation = Random.Range(0f, 2f);
        rigidbody.angularVelocity = new Vector3(angularRotation, angularRotation, angularRotation);
    }
}
