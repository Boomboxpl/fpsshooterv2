﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa definiująca ruch pocisków.
/// </summary>
public class Projectile : MonoBehaviour, DestroyAble
{
    /// <summary>
    /// Prefabrykat obiektu pojawiającego się po zniszczeniu pocisku.
    /// </summary>
    public GameObject hitmark;

    /// <summary>
    /// Szybkość pocisku.
    /// </summary>
    public float speed = 1;
    /// <summary>
    /// Maksymalny dystans.
    /// </summary>
    public float maxDistance = 10;
    /// <summary>
    /// Zadawane obrażenia.
    /// </summary>
    public float damage;
    /// <summary>
    /// Ile obrażeń jest w stanie wytrzymać przez zniszczeniem.
    /// </summary>
    public float durability;

    /// <summary>
    /// Kierunek poruszania się.
    /// </summary>
    protected Vector3 direction;

    /// <summary>
    /// Komponent ciała sztywnego.
    /// </summary>
    protected Rigidbody rigidbody;
    /// <summary>
    /// Przebyty dystans.
    /// </summary>
    private float traveledDistance = 0;
    
    /// <summary>
    /// Statyczna metoda tworząca poruszający się pocisk.
    /// </summary>
    /// <param name="prefab">Prefabrykat pocisku.</param>
    /// <param name="position">Pozycja początkowa.</param>
    /// <param name="direction">Kierunek.</param>
    /// <returns>Nowo utworzony obiekt pocisku.</returns>
    public static GameObject ThrowProjectile(GameObject prefab, Vector3 position, Vector3 direction)
    {
        var newProjectile = Instantiate(prefab, position, Quaternion.identity).GetComponent<Projectile>();
        newProjectile.SetRigidbody(direction);
        return newProjectile.gameObject;
    }

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    virtual public void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Aktualizacja co klatkę. Jeżeli przebyto maksymalny dystans, rozpocznie się spadanie.
    /// </summary>
    virtual public void Update()
    {
        traveledDistance += rigidbody.velocity.magnitude * Time.deltaTime;
        if(traveledDistance > maxDistance)
        {
            rigidbody.useGravity = true;
        }
    }

    /// <summary>
    /// Wykrywanie kolizji. Po kolizji pocisk zaczyna opadać. Inne pociski i przeciwnicy nie wykonują dodatkowej akcji.
    /// Każdy inny obiekt otrzyma obrażenia.
    /// </summary>
    /// <param name="collision">Informacje o kolizji.</param>
    virtual public void OnCollisionEnter(Collision collision)
    {
        rigidbody.useGravity = true;
        //Don't destroy on collision with enemy and other projectiles.
        if (collision.gameObject.GetComponent<Enemy>() || collision.gameObject.GetComponent<Projectile>())
        {
            return;
        }

        var targetToDestroy = collision.gameObject.GetComponent<DestroyAble>();
        if(targetToDestroy != null)
        {
            targetToDestroy.TakeDamage(damage, collision.collider.ClosestPoint(transform.position));
        }
        Die();
    }

    /// <summary>
    /// Odpowiednie ustawienie parametrów ciała sztywnego.
    /// </summary>
    /// <param name="targetDirection">Kierunek poruszania się.</param>
    virtual public void SetRigidbody(Vector3 targetDirection)
    {
        direction = targetDirection;
        rigidbody.velocity = direction * speed;
    }

    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    /// <param name="damage">Ilość otrzymanych obrażeń.</param>
    /// <param name="hitPoint">Miejsce otrzymania obrażeń.</param>
    public void TakeDamage(float damage, Vector3 hitPoint)
    {
        durability -= damage;
        if(durability < 0)
        {
            Die();
        }
    }

    /// <summary>
    /// Procedura umierania.
    /// </summary>
    private void Die()
    {
        if(hitmark != null)
        {
            Instantiate(hitmark, transform.position, Quaternion.identity);
        }
        
        Destroy(gameObject);
    }
}
