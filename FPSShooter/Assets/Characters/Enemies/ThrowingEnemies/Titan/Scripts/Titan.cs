﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa definiująca zachowanie Tytana (przeciwnik miotający) 
/// </summary>
public class Titan : ThrowingEnemy
{
    /// <summary>
    /// Funkcja edytora, pozwalająca na łatwe ustawienie zasięgu ataku.
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + Vector3.up, AttackDistance);
    }

    /// <summary>
    /// Inicjalizacja. Wybranie sposobu upadania. Rozpoczęcia pogoni za graczem.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        StartHuntPlayer(0.5f, 3f);
    }

    /// <summary>
    /// Ponowne rozpoczęcia pogoni za graczem, po zakończeniu ataku. Szybciej zaczyna biec niż przy pierwszej pogoni.
    /// </summary>
    protected override void EndOfAttackCallback()
    {
        StartHuntPlayer(0, 1f);
    }
}
