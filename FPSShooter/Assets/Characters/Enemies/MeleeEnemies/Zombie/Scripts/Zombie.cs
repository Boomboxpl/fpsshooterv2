﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa definiująca zachowanie Zombie(walczący wręcz).
/// </summary>
public class Zombie : MeleeEnemy
{
    /// <summary>
    /// Parametr Animatora określający, która z animacji upadania zostanie uruchomiona.
    /// </summary>
    [Header("Animator Mapping")]
    public string FallingDirectionAnimatorParameter = "IDOfFallingDirection";
    /// <summary>
    /// ID odpowiedniego parametru Animatora.
    /// </summary>
    private int FallingDirectionID;

    /// <summary>
    /// Funkcja edytora, pozwalająca na łatwe ustawienie zasięgu ataku.
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + Vector3.up, AttackDistance);
    }

    /// <summary>
    /// Inicjalizacja. Wybranie sposobu upadania. Rozpoczęcia pogoni za graczem.
    /// </summary>
    public override void Awake()
    {
        base.Awake();

        var fallingDirection = Mathf.FloorToInt(Random.Range(0, 2));
        FallingDirectionID = Animator.StringToHash(FallingDirectionAnimatorParameter);
        animator.SetInteger(FallingDirectionID, fallingDirection);

        StartHuntPlayer(0.5f, 1f);
    }

    /// <summary>
    /// Ponowne rozpoczęcia pogoni za graczem, po zakończeniu ataku. Szybciej zaczyna biec niż przy pierwszej pogoni.
    /// </summary>
    protected override void EndOfAttackCallback()
    {
        StartHuntPlayer(0f, 1f);
    }
}
