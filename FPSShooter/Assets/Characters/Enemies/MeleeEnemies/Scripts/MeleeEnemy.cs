﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa abstrakcyjna roszerzająca StandardEnemy o podstawowe właściwości potrzebne do zaimplementowania przeciwnika atakującego wręcz.
/// </summary>
public abstract class MeleeEnemy : StandardEnemy
{
    /// <summary>
    /// Ilość ataku.
    /// </summary>
    [Header("Attack Parameters")]
    [SerializeField]
    private float damage = 3;
    /// <summary>
    /// Czas pomiędzy rozpoczęciem ataku, i zadawaniem obrażeń.
    /// </summary>
    [SerializeField]
    private float attackOffsetTime = 0.7f;

    /// <summary>
    /// Inicjalizacja klas bazowych.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
    }

    /// <summary>
    /// Procedura ataku.
    /// </summary>
    public override void Attack()
    {
        if (!isAttacking)
        {
            StartCoroutine(AttackRoutine());
        }
    }

    /// <summary>
    /// Korutyna ataku. Wykonuje atak i po odczekaniu przesunięcia zadaje obrażenia graczowi, jeśli nadal znajduje się w zasięgu.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    protected override IEnumerator AttackRoutine()
    {
        animator.SetTrigger(IsAttackingID);
        isAttacking = true;

        yield return new WaitForSeconds(attackOffsetTime);
        if (IsPlayerInDistance(Vector3.up) && IsAlive)
        {
            //Deal damage
            player.TakeDamage(damage, player.transform.position);
        }
        EndOfAttackCallback();
        isAttacking = false;
        yield break;
    }
}
