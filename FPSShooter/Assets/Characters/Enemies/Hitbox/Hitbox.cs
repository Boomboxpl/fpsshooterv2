﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa rozrużniająca częsci ciała, które są atakowane.
/// </summary>
[RequireComponent(typeof(Collider))]
public class Hitbox : MonoBehaviour, DestroyAble
{
    /// <summary>
    /// Dodatkowy mnożnik ataku zadawana przeciwnikowi.
    /// </summary>
    [SerializeField]
    private float attackMultiplier = 1;

    /// <summary>
    /// Komponent wykrywania kolizji.
    /// </summary>
    private Collider collider;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    void Awake()
    {
        collider = GetComponent<Collider>();

        if(collider == null)
        {
            Debug.LogError("No collider in hitbox:" + gameObject.name);
        }
    }

    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    /// <param name="damage">Ilość otrzymanych obrażeń.</param>
    /// <param name="hitPoint">Miejsce otrzymania obrażeń.</param>
    public void TakeDamage(float damage, Vector3 hitPoint)
    {
        var enemy = GetComponentInParent<Enemy>();
        if(enemy != null)
        {
            enemy.TakeDamage(damage * attackMultiplier, hitPoint);
        }
        else
        {
            Debug.LogError("No enemy for hitbox");
        }
    }

    /// <summary>
    /// Reset do wartości domyślnych.
    /// </summary>
    private void Reset()
    {
        attackMultiplier = 1;
    }
}
