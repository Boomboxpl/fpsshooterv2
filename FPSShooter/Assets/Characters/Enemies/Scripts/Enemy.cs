﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstrakcyjna klasa przeciwnika.
/// </summary>
public abstract class Enemy : MonoBehaviour, DestroyAble
{
    /// <summary>
    /// Właściwość określająca poziom życia.
    /// </summary>
    public float Life { get; private set; }
    /// <summary>
    /// Właściwość określająca czy przeciwnik jest żywy.
    /// </summary>
    public bool IsAlive { get => Life > 0; }

    /// <summary>
    /// Prefabrykat obiektu pojawiającego się po trafieniu.
    /// </summary>
    [Header("Hit Marker")]
    public GameObject hitMarkerPrefab;

    /// <summary>
    /// Referencja do gracza.
    /// </summary>
    protected Player player;
    /// <summary>
    /// Referencja do komponentu zarządzającego poruszaniem się.
    /// </summary>
    protected CharacterController characterController;
    /// <summary>
    /// Referencja do komponentu odpowiedzialnego za dźwięk.
    /// </summary>
    protected CharacterAudioController characterAudio;

    /// <summary>
    /// Maksymalny dystans od gracza, potrzebny do wykonania ataku.
    /// </summary>
    [Header("Living Parameters")]
    [SerializeField]
    protected float AttackDistance = 2;
    /// <summary>
    /// Maksymalny poziom życia.
    /// </summary>
    [SerializeField]
    private float maxLife = 5;

    /// <summary>
    /// Przesunięcie w dół po śmierci.
    /// </summary>
    [Header("Dying Parameters")]
    [SerializeField]
    private float yDelta = 2;
    /// <summary>
    /// Czas pomiędzy rozpoczęciem procedury umierania i rozpoczęciem przesunięcia w dół.
    /// </summary>
    [SerializeField]
    private float waitTime = 1;
    /// <summary>
    /// Czas umierania.
    /// </summary>
    [SerializeField]
    private float timeOfDying = 1;
    /// <summary>
    /// Pole określające, czy obiekt umiera.
    /// </summary>
    private bool isDying = false;

    /// <summary>
    /// Abstrakcyjna metoda ataku, pozwalająca klasą pochodnym określić, co się dzieje podczas ataku.
    /// </summary>
    abstract public void Attack();


    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    virtual public void Awake()
    {
        Life = maxLife;
        player = FindObjectOfType<Player>();
        characterController = GetComponent<CharacterController>();
        characterAudio = GetComponent<CharacterAudioController>();
    }

    /// <summary>
    /// Aktualizacja co klatkę. Wywołuje atak, jeśli dystans do gracza jest dostateczny.
    /// </summary>
    virtual public void Update()
    {
        if(IsPlayerInDistance(Vector3.zero))
        {
            Attack();
        }
    }

    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    /// <param name="damage">Ilość otrzymanych obrażeń.</param>
    /// <param name="position">Miejsce otrzymania obrażeń.</param>
    virtual public void TakeDamage(float damage, Vector3 position)
    {
        Life -= damage;
        characterAudio.state = CharacterState.Suffering;
        Instantiate(hitMarkerPrefab, position, Quaternion.identity, transform);
        if (!IsAlive)
        {
            Die();
        }
    }

    /// <summary>
    /// Procedura umierania.
    /// </summary>
    virtual public void Die()
    {
        characterAudio.state = CharacterState.Dying;
        StartCoroutine(Dying());
    }

    /// <summary>
    /// Aktualizacja dźwięku.
    /// </summary>
    virtual protected void UpdateSound()
    {
        characterAudio.UpdateSound();
    }

    /// <summary>
    /// Sprawdzenie czy gracz znajduje się odległości do ataku.
    /// </summary>
    /// <param name="offset">Przesunięcie punktu mierzenia odległości.</param>
    /// <returns>True, gdy dystans jest mniejszy.</returns>
    public bool IsPlayerInDistance(Vector3 offset)
    {
        if(player == null || player.enabled == false)
        {
            return false;
        }
        return Vector3.Distance(player.transform.position, transform.position + offset) < AttackDistance;
    }

    /// <summary>
    /// Korutyna umierania.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator Dying()
    {
        if (!isDying)
        {
            isDying = true;
            GetComponent<CharacterController>().enabled = false;
            var timeRemain = timeOfDying;
            yield return new WaitForSeconds(waitTime);
            while ((timeRemain -= Time.deltaTime) > 0)
            {
                transform.Translate(yDelta * Vector3.down * (Time.deltaTime / timeOfDying), Space.World);
                yield return null;
            }
            GameManager.Instance.EnemyKilled();
            Destroy(gameObject);
        }
    }
}
