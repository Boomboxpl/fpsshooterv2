﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa abstrakcyjna rozszerzająca Enemy o standardowe działania przecwnika z animacją zdefiniowaną w Animatorze.
/// </summary>
public abstract class StandardEnemy : Enemy
{
    /// <summary>
    /// Nazwa odpowiedniego prametru w animatorze.
    /// </summary>
    [Header("Animator Mapping")]
    public string IsAliveAnimatorParameter = "IsAlive";
    /// <summary>
    /// Nazwa odpowiedniego prametru w animatorze.
    /// </summary>
    public string IsAttackingAnimatorParameter = "IsAttacking";
    /// <summary>
    /// Nazwa odpowiedniego prametru w animatorze.
    /// </summary>
    public string IsMovingAnimatorParameter = "IsMoving";
    /// <summary>
    /// Nazwa odpowiedniego prametru w animatorze.
    /// </summary>
    public string IsRunningAnimatorParameter = "IsRunning";

    /// <summary>
    /// Prędkość chodzenia.
    /// </summary>
    [Header("Moving Parameters")]
    public float walkingSpeed = 2.5f;
    /// <summary>
    /// Prędkość biegu.
    /// </summary>
    public float runningSpeed = 6.5f;

    /// <summary>
    /// Pole określające czy obiekt się porusza.
    /// </summary>
    protected bool isMoving = false;
    /// <summary>
    /// Pole określające czy obiekt biegnie.
    /// </summary>
    protected bool isRunning = false;
    /// <summary>
    /// Pole określające czy obiekt atakuje.
    /// </summary>
    protected bool isAttacking = false;

    /// <summary>
    /// Komponent animatora.
    /// </summary>
    protected Animator animator;

    /// <summary>
    /// ID w animatorze. Ustalane podczas inicializacji.
    /// </summary>
    protected int IsAliveID;
    /// <summary>
    /// ID w animatorze. Ustalane podczas inicializacji.
    /// </summary>
    protected int IsAttackingID;
    /// <summary>
    /// ID w animatorze. Ustalane podczas inicializacji.
    /// </summary>
    protected int IsMovingID;
    /// <summary>
    /// ID w animatorze. Ustalane podczas inicializacji.
    /// </summary>
    protected int IsRunningID;

    /// <summary>
    /// Uchwyt uruchomionej korutyny podążania za graczem.
    /// </summary>
    private Coroutine activeHunt;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public override void Awake()
    {
        base.Awake();
        animator = GetComponent<Animator>();

        IsAliveID = Animator.StringToHash(IsAliveAnimatorParameter);
        IsAttackingID = Animator.StringToHash(IsAttackingAnimatorParameter);
        IsMovingID = Animator.StringToHash(IsMovingAnimatorParameter);
        IsRunningID = Animator.StringToHash(IsRunningAnimatorParameter);

        animator.SetBool(IsAliveID, IsAlive);
        animator.SetBool(IsMovingID, false);
        animator.SetBool(IsRunningID, false);
    }

    /// <summary>
    /// Aktualizacja co klatkę. Aktualizuje dźwięk oraz pogoń za graczem.
    /// </summary>
    public override void Update()
    {
        base.Update();
        if (player == null)
        {
            isMoving = false;
            isRunning = false;
            return;
        }

        UpdateMove();

        UpdateSound();
    }

    /// <summary>
    /// Callback wywoływany zaraz po ataku. Określa co przeciwink ma wykonać, gdy już skończył atakować.
    /// </summary>
    protected abstract void EndOfAttackCallback();

    /// <summary>
    /// Korutyna ataku. Definiowana przez klasy pochodne.
    /// </summary>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    protected abstract IEnumerator AttackRoutine();

    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    /// <param name="damage">Ilość otrzymanych obrażeń.</param>
    /// <param name="position">Miejsce otrzymania obrażeń.</param>
    public override void TakeDamage(float damage, Vector3 position)
    {
        base.TakeDamage(damage, position);
        animator.SetBool(IsAliveID, IsAlive);
    }

    /// <summary>
    /// Rozpoczęcie pogoni za graczem. Przrywa aktywną pogoń.
    /// </summary>
    /// <param name="walkStartTime">Czas do rozpoczęcia marszu.</param>
    /// <param name="runStartTime">Czas do rozpoczęcia biegu.</param>
    virtual public void StartHuntPlayer(float walkStartTime, float runStartTime)
    {
        if(activeHunt != null)
        {
            StopCoroutine(activeHunt);
        }

        activeHunt = StartCoroutine(HuntPlayer(walkStartTime, runStartTime));
    }

    /// <summary>
    /// Aktualizacja stanu dźwięku.
    /// </summary>
    protected override void UpdateSound()
    {
        if (characterAudio.state == CharacterState.Idle && IsAlive)
        {
            if (isMoving)
            {
                characterAudio.state = CharacterState.Walking;
            }

            if (isRunning)
            {
                characterAudio.state = CharacterState.Running;
            }

            if (isAttacking)
            {
                characterAudio.state = CharacterState.Attacking;
            }
        }
        base.UpdateSound();
    }

    /// <summary>
    /// Aktualizacja poruszania się.
    /// </summary>
    virtual protected void UpdateMove()
    {
        var playerPosition = player.transform.position;
        //Look at player
        transform.LookAt(new Vector3(playerPosition.x, transform.position.y, playerPosition.z));

        if (!isAttacking)
        {
            var directionXZ = new Vector2(playerPosition.x - transform.position.x, playerPosition.z - transform.position.z).normalized;
            var direction = new Vector3(directionXZ.x, -9.81f, directionXZ.y);
            var speed = 0.0f;
            if (isMoving)
            {
                speed = walkingSpeed;
            }
            if (isRunning)
            {
                speed = runningSpeed;
            }
            if (characterController.enabled)
                characterController.Move(speed * direction * Time.deltaTime);
        }
    }

    /// <summary>
    /// Korutyna pościgu za graczem.
    /// </summary>
    /// <param name="walkStartTime">Czas do rozpoczęcia marszu.</param>
    /// <param name="runStartTime">Czas do rozpoczęcia biegu.</param>
    /// <returns>Patrz działanie korutyn w Unity.</returns>
    private IEnumerator HuntPlayer(float walkStartTime, float runStartTime)
    {
        isMoving = false;
        isRunning = false;
        yield return new WaitForSeconds(walkStartTime);
        isMoving = true;
        animator.SetBool(IsMovingID, true);
        yield return new WaitForSeconds(runStartTime);
        isRunning = true;
        animator.SetBool(IsRunningID, true);
    }
}
