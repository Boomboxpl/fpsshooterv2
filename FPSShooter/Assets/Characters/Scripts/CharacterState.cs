﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Typ wyliczeniowy reprezentujący aktualny stan ruchu postaci.
/// </summary>
public enum CharacterState
{
    /// <summary>
    /// Bezczynny.
    /// </summary>
    Idle,
    /// <summary>
    /// Marsz.
    /// </summary>
    Walking,
    /// <summary>
    /// Bieg.
    /// </summary>
    Running,
    /// <summary>
    /// Skok.
    /// </summary>
    Jumping,
    /// <summary>
    /// Lądowanie.
    /// </summary>
    Landing,
    /// <summary>
    /// Atakowanie.
    /// </summary>
    Attacking,
    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    Suffering,
    /// <summary>
    /// Umieranie.
    /// </summary>
    Dying
}
