﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa zarządzająca odtwarzanym przez postacie dzwiękiem, na podstawie stanu.
/// </summary>
public class CharacterAudioController : MonoBehaviour
{
    /// <summary>
    /// Stan poruszania się.
    /// </summary>
    public CharacterState state;

    /// <summary>
    /// Dźwięk chodzenia.
    /// </summary>
    [SerializeField]
    private AudioClip walkingSound;
    /// <summary>
    /// Dźwięk biegu.
    /// </summary>
    [SerializeField]
    private AudioClip runningSound;
    /// <summary>
    /// Dźwięk skakania.
    /// </summary>
    [SerializeField]
    private AudioClip jumpingSound;
    /// <summary>
    /// Dźwięk lądowania.
    /// </summary>
    [SerializeField]
    private AudioClip landingSound;
    /// <summary>
    /// Dźwięk atakowania.
    /// </summary>
    [SerializeField]
    private AudioClip attackingSound;
    /// <summary>
    /// Dźwięk otrzymywania obrażeń
    /// </summary>
    [SerializeField]
    private AudioClip sufferingSound;
    /// <summary>
    /// Dźwięk umierania.
    /// </summary>
    [SerializeField]
    private AudioClip dyingSound;

    /// <summary>
    /// Tablica dźwięków.
    /// </summary>
    private AudioClip[] sounds;
    /// <summary>
    /// Komponent odtwarzający dźwięk.
    /// </summary>
    private AudioSource audioSource;

    /// <summary>
    /// Inicjalizacja
    /// </summary>
    private void Awake()
    {
        sounds = GetSoundsArray();
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
    }

    /// <summary>
    /// Aktualizuje dźwięk względem stanu.
    /// </summary>
    virtual public void UpdateSound()
    {
        AudioClip choosedClip = sounds[(int)state];
        state = CharacterState.Idle;
        PlaySound(choosedClip);
    }

    /// <summary>
    /// Odhrywa dźwięk. Gdy ścieżka jest taka sama jak aktualnie odtwarzany dźwięk, nic nie jest zmieniane.
    /// Pusta ścieżka jest pomijana.
    /// </summary>
    /// <param name="clip">Ścieżka do odtworzenia.</param>
    private void PlaySound(AudioClip clip)
    {
        if (audioSource.clip != clip || !audioSource.isPlaying)
        {
            if (clip != null)
            {
                audioSource.clip = clip;
                audioSource.Play();
            }
        }
    }

    /// <summary>
    /// Tworzy tablice dzwięków, na podstawie ustawionych w edytorze, co pozwala na łatwiejszy odczyt, względem stanu.
    /// </summary>
    /// <returns>Tablica ścieżek dzwiękowych</returns>
    private AudioClip[] GetSoundsArray()
    {
        return new AudioClip[] { null, walkingSound, runningSound, jumpingSound, landingSound, attackingSound, sufferingSound, dyingSound };
    }
}