﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interferjs dla obiektów, mogących przyjmować obrażenia.
/// </summary>
public interface DestroyAble
{
    /// <summary>
    /// Otrzymywanie obrażeń.
    /// </summary>
    /// <param name="damage">Ilość otrzymanych obrażeń.</param>
    /// <param name="hitPoint">Miejsce otrzymania obrażeń.</param>
    void TakeDamage(float damage, Vector3 hitPoint);
}
