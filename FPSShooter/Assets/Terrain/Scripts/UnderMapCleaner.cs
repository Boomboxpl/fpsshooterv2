﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Klasa czyszcząca wszystkie obiekty, które spadły z mapy.
/// Pod każdą mapą znajduje się duży obszar, w którym obiekty są niszczone.
/// </summary>
public class UnderMapCleaner : MonoBehaviour
{
    /// <summary>
    /// Wykrycie wejscia do obszaru niszczenia. Zadaje ogromne obrażenia obiektom DestrotAble. Niszczy pozostałe typy obiektów.
    /// </summary>
    /// <param name="other">OBiekt, który znalazł się w obszarze.</param>
    private void OnTriggerStay(Collider other)
    {
        var destroyable = other.gameObject.GetComponent<DestroyAble>();
        if(destroyable != null)
        {
            destroyable.TakeDamage(float.MaxValue, Vector3.zero);
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
