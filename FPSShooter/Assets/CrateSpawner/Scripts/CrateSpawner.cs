﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Zarządca tworzenia skrzynek.
/// </summary>
public class CrateSpawner : MonoBehaviour
{
    /// <summary>
    /// Fabryki skrzynek. Odczytywane jako komponenty obiektu.
    /// </summary>
    private CrateFactory[] crateFactories;

    /// <summary>
    /// Ostatni użyty indeks miejsca pojawienia się skrzynek.
    /// </summary>
    private int lastIndex = -1;

    /// <summary>
    /// Inicjalizacja.
    /// </summary>
    public void Awake()
    {
        RefreshFactories();
    }

    /// <summary>
    /// Funkcja edytora, pozwalająca na łatwe ustawienie puntków pojawiania się skrzynek.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        foreach(Transform child in transform)
        {
            Gizmos.DrawSphere(child.position, 10);
        }
    }

    /// <summary>
    /// Procedura tworzenia skrzynek.
    /// </summary>
    public void SpawnCrates()
    {
        foreach(var crateFactory in crateFactories)
        {
            var crateTransform = crateFactory.CreateCrate().transform;
            crateTransform.position = GetRandomPositionFromChild();
        }
    }

    /// <summary>
    /// Odświeżenie używanych fabryk skrzynek.
    /// </summary>
    public void RefreshFactories()
    {
        crateFactories = GetComponents<CrateFactory>();
    }

    /// <summary>
    /// Uzyskanie losowej pozycji, wybranej z pozycji dzieci obiektu.
    /// </summary>
    /// <returns>Losowa pozycja.</returns>
    private Vector3 GetRandomPositionFromChild()
    {
        var childIndex = -1;
        do 
        {
            childIndex = Random.Range(0, transform.childCount);
        } while (childIndex == lastIndex);

        lastIndex = childIndex;

        return transform.GetChild(childIndex).position;
    }
}
